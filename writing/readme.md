## Site Build

This is the repository for the Xalgorithms Foundation website.

This site is built in [next.js](https://nextjs.org/). It uses [xf-material-components](https://gitlab.com/xalgorithms-alliance/component-documentation), the Xalgorithms Foundation Material Components.

To develop locally use `yarn dev`, or `yarn build` and then `yarn start`

## Contributing 

Edits and new pieces of writing are always welcome.

Please commit or target pull requests to the `drafts` branch. You can navigate to the `drafts` branch by selecting it from the branch dropdown that will, by default, be labeled as `master`.

If you have general questions about writing, style, process, etc. checkout the [writing guidelines document](https://hungry-saha-d9d963.netlify.app/writing).

See the `/writing` folder for specific instructions about contributing blog posts

## Pages

We're looking for contributors to provide translations for the main pages on the website.

This content is contained in the `content` directory. Each file exports a json object that contains all the content for an individual page.

To contribute a translation, create an object named `the-language-you-are contributing`. Within this, copy the existing English language data structure and supply your translation using this as the scaffold.

## Blog Posts

Blog posts are written in an `.mdx` format. [Mdx](https://mdxjs.com/) allows you to use React components in markdown. This affords us the convinience of simple mark down syntax as well as the ability to bring in logic in the form of React components.

You can use all basic markdown syntax as well as [github flavored markdown](https://github.github.com/gfm/).
  
Additionally you can uses custom components for footnotes and photo captions.


```
<SideQuote>This text will appear as a footnote and will be automatically numbered according to the number of sidequote modules included in the piece file</SideQuote>  

<Caption>Your photo caption goes here</Caption>

```

## Build Pipelines

You can visit the live website [here](https://hopeful-blackwell-2d9bad.netlify.app/).

You can see the drafts branch live [here](https://objective-villani-f45a00.netlify.app/).
