---
title: 'The Data With Direction Specification (DWDS) in a Nutshell'
date: '2021-05-26'
featimg: 'writing-images/RULE-OUGHT.png'
featimgalt: ''
summary: ''
author: 'Joseph Potvin'
authorProfile: ''
authorDescription: ''
tags: [
    {tag: 'DWDS' },
]
version: '1.0'
published: 'true'
---

No one can derive OUGHT from IS, [as David Hume argued](https://plato.stanford.edu/entries/hume-moral/#io) three centuries ago. 

But anyone can assert OUGHT from IS. That's called a 'RULE'. In every society, organization and household, each person is both a rule-maker and a rule-taker as we routinely use terms such as MUST, MAY, and SHOULD, or their various negatives and synonyms.

Let's describe those two roles very generally as follows:
* A 'rule-maker' is any person (individual or entity) asserting an obligation, permission, or encouragement
* A 'rule-taker' is any person (individual or entity) deemed subject to obligation, permission or encouragement

Of course, we can distinguish informal and formal rules:
* Informal IS + RULE ⟾ OUGHT assertions are opinions and attitudes that arise in cultural and interpersonal relationships.
* Formal IS + RULE ⟾ OUGHT assertions are composed as laws, contracts, specifications, administrative policies, treaties, standards, and similar documents. 

The Latin word for rule is *norma*, and statements about rules are referred to in the philosophical literature as *normative propositions*. Our general-purpose design includes normative propositions concerned with utility, logic, ethics or aesthetics. (Some of the literature specializes in *deontic propositions*, derived from the Greek term δεον, which means ‘appropriate’ in relation to ethics.) 

In the terminology of our framework, we say that a formal rule is ‘in effect’ for a context of dates/times and prerogatives based upon identity and jurisdiction. And we say that a rule is ‘applicable’ to certain general categories of activities, things, and/or purposes. Further, when particular events or circumstances unfold that correspond to some particular scenario described in a rule, we say that the rule is 'invoked'. This is relevant to any domain that is subject to obligation, permission, or expectation. Here we speak in the abstract instead of through examples, in order to emphasize the intended comprehensive utility of the approach. 

Each rule is based upon some type of agreement, authority, resolve, or preference which persists through a period of time. In varying degrees, a person may like or dislike a rule; advocate or challenge it; conform with or evade it; understand or be confused by it. But prior to all of that, a person would have to become aware of a rule for such a rule to exist, before then can react to it. We use the expression 'IS + RULE ⟾ OUGHT' to refer generically to any assertion that *a particular factual circumstance invokes a certain set of obligations, permissions or expectations*. The symbol ⟾  is not intended to just mean *therefore*. In our system, the symbol ⟾   means *when there is a known rule which is 'in effect', 'applicable' and 'invoked', therefore*. To avoid confusion it's best to use the full expression 'IS + RULE ⟾ OUGHT' than a short form like 'IS ⟾ OUGHT'. 

From this we can percieve a rule as a type of message between a rule-maker and a rule-taker. We would go so far as to day that a rule statement that is not communicated, does not yet exist as a rule, in the same way that a message statement, uncommunicated by the author to someone else, is not yet a message. 

And this begs the question: How does anyone obtain awareness of formal IS + RULE ⟾ OUGHT assertions? 

The Data With Direction Specification (DWDS) is a new general-purpose request-response messaging system for use across any digital network. It is designed to make it easy for anyone to publish, discover, fetch, scrutinize and prioritize rules in a way that can be directly read and understood by non-specialized humans and machines, for any purpose, in any language. The purpose of this system is to enhance the functional communication of IS + RULE ⟾ OUGHT assertions with precision, simplicity, scale, volume and speed. This system accommodates a hierarchy of rules, the sense that there are meta-rules which provide structure to rule-making itself, based upon jurisdiction and identity. In law this is refered to as 'prerogative'.

DWDS is a four-part specification, consisting of: the RuleData data model; the RuleMaker application design; the RuleTaker component design; and, the RuleReserve data storage design. Any person (individual or entity) can operate all or some of these elements at their point of connection ('node'), where all of these nodes taken together comprise a peer-to-peer network for the exchange of IS + RULE ⟾ OUGHT data. Each of these four parts is passive, which is to say that every action is initiated by a human agent through a node, or by their authorized intermediary machine or application. The network is designed to be incapable of initiating action.

DWDS serves a communication facilitation role, to transmit information among users of nodes on this network, with no network capability to impose, enforce, or monitor. This is deliberate. From the philosophical core through to the working reference implementations, DWDS is premised upon IS + RULE ⟾ OUGHT assertions being subject to the complexities of interpretation and context. In a milieu of diverse parties, unknown scenarios, and externally defined rule structures and sequences, it is essential that each person deemed to be subject to a rule determines whether or not, and to what degree, they shall proceed in accordance with it. This is a logical necessity of human-centred design. An Internet of Rules requires inviolable rule-taker agency, unlike systems that would use mechanistic command procedures, or semi-autonomous (so-called) artificial intelligence. 

The four parts of this specification are managed as discrete sub-projects of the DWDS Working Group, hosted by Xalgorithms Foundation. To introduce the design, I'll provide below a general functional summary. The more thorough descriptions are available in [the core DWDS paper](https://xalgorithms.org/white-paper). We will wrap up with a few highlights arising from DWDS's unique design.

#### A Functional Walk-Through

##### **DWDS RuleData Specification** 

Normative propositions (i.e. statements involving MUST, MAY and SHOULD, or their negatives or synonyms) are packaged in the DWDS as a distinct class of data. Each record contains metadata, input/output and reference data, and some additional descriptive fields expressed in a simple tabular declarative form. The records are structured for efficient storage, rapid sifting, and low-latency transmission. Most importantly, data, logic and procedure are unbundled. Therefore, the data is comprehensible for display on end-user devices, for integrity auditing, as well as for dynamic incorporation into applications on any platform. When accompanied by a parsing library, these rule records can be queried from, or embedded into, or auto-transcribed into any programming language. 
 
##### **DWDS RuleMaker Specification**  

RuleMaker describes an easy-to-use application for authoring, publishing, and maintaining rules and their reference tables, along with portfolio management, and storage/retrieval of rule records and reference data records with the repository. RuleMaker does not require the rules author to write any programming code, or to know the nuances of algorithmic logic. Instead, someone using RuleMaker is provided a fixed-grammar template with just six syntactic elements to create the particular type of declarative sentence for the Input Conditions and Output Assertions. Only three of the syntactic elements are required; the other three enable greater data precision. RuleMaker output conforms with the RuleData specification and is ready to be published to the RuleReserve network, a sort of digital library.  All rules and reference data records are maintained in the author's own online versioning repository.
 
##### **DWDS RuleReserve Specification**  

Independently-operated RuleReserve instances comprise nodes of a decentralized network so that any participant can self-provision an IS + RULE ⟾ OUGHT service over the Internet. Any person can jointly host a distributed library of records and supporting reference data records. Each rule record occupies a single row (referred to as [rule.ior] record) of a large consolidated matrix [m x n] (i.e. rows x columns) for compact storage, and fast, high-volume data sifting. Each RuleReserve node is equipped for request-response messaging. A RuleReserve instance receives structured messages containing factual data describing a circumstance (referred to as a [is.ior] message), which is pre-configured as to sift through all the rows for rules that are ‘in effect’ and ‘applicable’ to the contents of that message. Rows remaining from the two-cycle sifting operation are packaged into an response message (called [ought1.xa] and [ought2.ior]) that is returned to the particular RuleTaker which issued the original [is.ior] request. In sum: RuleReserve uses [is.ior] requests to sift through [rule.ior] records to assemble and return [ought.xa] responses. A RuleReserve network node has no capability to initiate action; it just receives [rule.xa] records that authors have uploaded to [rulereserve.ior] from RuleMaker instances, and it receives [is.ior] request messages that it employs as simple sieves to quickly generate [ought1.ior] response messages. The RuleReserve does not process logic gates that [rule.ior] records contain, because they may involve private or confidential information that must be under the control of end-users. It is left to the operators of RuleTaker components to determine exactly what rule results are invoked.
 
##### **DWDS RuleTaker Specification**  

The RuleTaker specification can be implemented and used as a stand-alone app or as an auxiliary component or service with any other platform. Its purpose is to enable a person (individual or entity) to fetch, scrutinize, prioritize, and at their option to computationally process normative rules and related reference tables that have been obtained from the RuleReserve network. The [is.ior] message that was used on RuleReserve nodes to sift for rules that are 'in effect' and 'applicable', is used again locally to sift though the logic gates of each of the rules in the [ought.xa] message. RuleTaker rapidly compares the contents of that [is.ior] message to the Input Conditions of each rule in order to identify the matching scenario column(s), and this reveals which of the Output Assertions are ‘invoked’, as well and providing the normative mode. This result can then be presented to the user directly, or via the platform they are using. This RuleTaker component has no capability to initiate an action to reach out and get data; it is only able to respond to events data supplied through user action or configuration.
 
#### Some Design Highlights
 
The whole of the DWDS system is more that the sum of its parts. Some very interesting concepts and capabilities emerged from deliberations among community contributors.

One of the most useful outcomes is that DWDS enables full multilingual equivalency for the expression and sharing of rules. Each row label with an Input Condition or an Output Assertion contains a complete sentence, but all of these sentences share the same syntactic structure, based on six elements. The sentences can be in any natural language for human comprehension. Since they are always pre-parsed into the same six syntactic elements, data processing is efficient. A novice rule author need only be provided a few examples in any language they are comfortable with, and they would be able use the six syntactic elements provided in RuleMaker to create their own structured sentences in any other language they know, using any character set, with elements displayed in the orientation, order and flow direction of the language in use (e.g. horizontal left-to-right; horizontal right-to-left; vertical top-to-bottom). The multilingual character of the DWDS enables an Internet of Rules that is linguistically and culturally pluralist. 

We are also pleased with the very simple but information-dense input/output logic gate that emerged from a blend of thoughtful team discussion and deep-dive research. This is structured as a quaternary *bi-lattice*, that's to say, a table with two sets of semantic meanings assigned to each of its four symbols. Input Conditions are associated with combinations and permutations of {00,01,10,11} to signify {No, Yes, Yes AND No, Yes OR No}. Output Assertions are associated with combinations and permutations of {00,01,10,11} meaning {NOT, MUST, MAY, SHOULD}. This reduces the computational burden enormously.
 
Finally, at a very broad conceptual level, DWDS holds Artificial Naïvety (A∅, eh-nought) to be a mandatory system criterion. All concerns relating to context, comprehension, purpose, reason, learning, or motivation are end-user prerogatives, and all of the system’s data, metadata and activity logs belong to end-users. An A∅ system has no methods to retain or copy data or metadata. A RuleReserve node occupies itself with each run-time request-response signal that is being processed and does not store those details. This design aligns to the well-known axiom of Internet architecture that: “The Internet has smart edges... and a simple core", [IETF RFC3439. Some Internet Architectural Guidelines and Philosophy](https://tools.ietf.org/html/rfc3439#page-3). This is a very different approach from Artificial intelligence (AI) which involves active knowledge acquisition, inductive reasoning, stochastic variational inference, and double-loop learning. The two are not mutually exclusive. Any person may choose to use DWDS's A∅ together with their favourite AI platform, but DWDS is designed to be incapable of such monitoring.
