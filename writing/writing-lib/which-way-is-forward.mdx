---
title: 'Which Way is Forward?'
date: '2021-07-11'
featimg: 'writing-images/direction.png'
featimgalt: ''
summary: ''
author: 'Joseph Potvin'
authorProfile: ''
authorDescription: ''
tags: [
    {tag: 'DWDS' }
]
version: '1.0'
published: 'true'
---


<a href="https://gitlab.com/xalgorithms-alliance/dwds-documents/-/tree/master/current" target="_blank">
<button className="altbutton">Read the White Paper</button>
</a>
<div style={{height: '0.5em'}}/>

A little while ago I prepared a short analysis about a common contradiction in economic perception, and I set off on a quest to fix it. I'm tempted to discuss that scenario here, as well as numerous related examples. But instead I'll keep this abstracted to the generic class of problem, and invite you to consider the essence of the matter in relation to your own perspective and experiences.

The general problem to solve in my case, as in many scenarios, had to do with how we communicate which way is 'forward' when orienting decisions involving many people.

A century ago, philosopher Jerzy (Georges) Kalinowski [distinguished *normative* relationships](https://www.researchgate.net/publication/263238296) between agents and their actions as those which involve obligation, permission and encouragement. Kalinowski's use of the term *normative* was rooted in *norma*, the Latin word for *rule*. A rule is any practical, logical, ethical or aesthetic direction established and communicated among two or more people. Direction in what we 'ought' (or 'ought not') to do, to be, or to have is expressed in natural language with terms like MUST, MAY and SHOULD, or their negatives and synonyms. The connection with the administrative position of "Director" in an organization is straightforward. Even when there is disagreement about which rules various parties want to guide decisions, or about what is going on that would invoke certain decisions, there is nevertheless an underlying human sense that normative direction is an essential factor in action.

We can represent this with the expression: 'IS + RULE ⟾ OUGHT'. When particular events or circumstances occur that correspond to the Input Conditions of some rule, then one or more Output Assertions are 'invoked'.

In fields that you know most about, how exactly does one find out about what they MUST, MAY and SHOULD do, or be, or have?  Who exactly has the prerogative to establish this or that rule? And when they create a new rule, or when an existing rule is changed, exactly how are all the others, who are presumed to be subject to it, supposed to learn about it?

My reflection upon the original problem that I wanted to solve brought me to the view that available systems for communicating normative direction were haphazard, leading to all sorts of unintended consequences for on-going micro-level decisions, as well as for core macro-level system design incentives and mechanisms.

So, I began a process of design research which encompasses conceptualization, specification, and implementation. Here, I'll skip over the steps and jump staight to the outcome. I wrote a rather lengthy paper entitled: *"Data With Direction: IS + RULE ⟾ OUGHT"* which explains a new general-purpose method to improve the communication of rules. In this design, normative data is a distinct class of data that can be instantaneously discovered and transmitted over the Internet, in a manner that is usable by non-specialized humans and machines, for any purpose, in any language.

I began the academic part of this journey eight years ago because I felt it required more conceptual rigour than I'd be able to accomplish in the domains of business, government or civil society organizations. It's in my nature to excavate problems all the way down to bedrock theory, then build my way back up again with a solid conceptual structure and a general purpose solution to the entire class of problem. The DWDS paper is the central portion of my 'design research' doctoral dissertation at the Université du Québec. If my only purpose were to get the degree, I'd have it in hand already, but this dissertation does double-duty as a specification for three genuine reference implementations. In high-performance industries, a system specification is not considered complete until at least three working implementations meet all the requirements using different technologies on different platforms.

When designing something novel, one needs to explain how it would work; clarify the principles that enable it to work; make the case why others would want it to work; and find people and organizations with the wherewithal to jointly commit towards making it work. And then, of course, you have to actually make it work!

Regarding those last two points, it was my good fortune at the end of 2014 that I met Bill Olders. He was looking for someone to help achieve a closely related goal that he had been trying to advance for decades. Immediately both he and I could see that what my effort lacked, he had; and what his effort lacked, I had. There was no time to lose! Within a week we signed a contract to create the core system on free/libre/open terms, which combine active respect for *user freedoms* (demand-side) with persistent conformance to *transparent* methods of production and provisioning (supply-side). We devoted 2015 to narrowing the scope of what we would jointly design, which is to say, we carved away what we would not do. This essential principle was expressed by Antoine de Saint-Exupéry: "In anything at all, perfection is finally attained not when there is no longer anything to add, but when there is no longer anything to take away."

In 2016 we described "an Internet of Rules", and with angel funding from Bill's company DataKinetics, we incorporated Xalgorithms Foundation to host the free/libre/open source work, and we began to assemble a team. Full-stack designer/developer Don Kelly was one of our first contracted team members, and he remains our lead implementer. He patiently interprets and fills in many gaps between my descriptions and the rapidly evolving technology available to us for implementation.  

But it took another five years to complete the circle. Principal contributors of ideas integrated into this specification are listed as co-authors of the paper, and below I'd like to offer some acknowledgement:
* By convention, one's academic research supervisor is listed last, but my gratitude to Dr. Stéphane Gagnon at Université du Québec is more significant than such a placement might indicate. He's been an excellent coach who has deftly balanced persuasion and freedom.
* My incrementally advancing functional design was accompanied by inspired technical insights and iterations of working software by Don. The components, we came to be called RuleMaker, RuleReserve and RuleTaker, arose from a lot of back-and-forth with him to delineate the concept of operations.
* Since 2017 Craig Atkinson has contributed pragmatic descriptions regarding how this method could streamline cross-border trade rules. His steady steam of high-quality business articles and outreach have opened up many conversations with people in business, government, civil society organizations, and academia.
* In 2019 Ryan Fleck attended an event that I co-hosted with Richard Stallman of the Free Software Foundation, and ever since, Ryan has helped out on various parts of the implementation, most recently as developer of successively improved versions of the RuleMaker application.
* Also in 2019, creative technologist Calvin Hutcheon approached me at a futures conference, and offered to structure Xalgorithms Foundation's diverse initiatives into a single coherent portfolio and brand. His clear vision crystalized many aspects of the project in a way that enabled me to understand my own design better. Then he created an intuitive user interface for RuleMaker that transforms it into an easy-to-approach sense-making workspace.
* Wayne Cunneyworth's guidance has provided a deeper grasp of the tabular data processing method that I had been learning from Bill, which is core to the data sifting in RuleReserve nodes for rules metadata for those which are 'in effect' and 'applicable', as well as to RuleTaker components that sift through logic gates for rules that are 'invoked'. Wayne's recommendation in mid-2020 to formal quaternary logic put us on track to dramatically simplify rule expression, to elegantly incorporate uncertainty, to reduce the amount of programming code required for implementation, and to strongly boost speed and performance.
* My immediate family has been helpful too. My engineer son Julian helped to weed the thought garden. My pilot daughter Sara suggested the direct pedagogical potential of RuleMaker, and also its utility for aviation rules. And my anthropologist spouse, Angela, transformed my quick little test of the sentence structure in our logic gates with two or three other languages, into a fascinating exploration of the same syntactic structure in 25 linguistic/cultural contexts. This resulted in one of the most significant innovations of the design: substantive multilingual equivalency for the expression and processing of normative data.
* The 20-page list of literature references appended to the formal paper is intended as a genuine note of thanks to the many authors whose work has influenced the design.
* There are quite a few others whose contributions I should also acknowledge, but I'll do so in later blog posts where I'll be discussing their ideas.

The whole community has been collaborating on the basis of the famous principle of the Internet Engineering Task Force (IETF) introduced by David Clark in the 1980s: [rough consensus and running code](https://www.ietf.org/how/runningcode/). Of course, the usual caveat applies; any errors and omissions are entirely mine.

It will be useful here to offer some thoughts about the particular function of the core paper: what it is, and what it is not.

*Data With Direction: IS + RULE ⟾ OUGHT* is an initial comprehensive description of the general purpose, end-to-end method. It is *first* an academic document to provide the results of theoretical and applied design research, and it is the central section of my dissertation.

It performs a *secondary* duty as a general specification that supplies just enough detail for the concurrent elaboration of three reference implementations. It's a Version 0.x reference document for beta system builders. It will be Version 1.0 once we have all three reference implementations operational.

As a design research document, there is heavy emphasis on the reasons for key choices. Ongoing peer-review during the design research process provided many opportunities for discussion. There's always more than one way to do something 'correctly', so on several points I summarize alternatives, and provide reasons for taking a particular path.

I mentioned above that the paper is intentionally abstracted from any particular implementation technology, and from any particular use case domain context. In the past when working on general-purpose solutions, I've found that as soon as I'd say: "For example, suppose ...", immediately the sense of generality is lost because the idea takes root in the reader's mind in a particular context. It usually is helpful to ground an idea with tangible illustrations, certainly, and I commonly do. But my instinct is to maintain *this* one in the abstract. Therefore examples in the core paper are minimal. Admittedly, such abstraction can make the design a little harder to relate to initially. But I hope this also makes it easier to consider for purposes, and in scenarios, and on platforms that I've not thought of. 

Several people have informed me that the style of the core paper is very dense, thus slow to work through. I make no pretense that it is a marketing document, other than in the sense that those who would prefer just the top layer will also tend to want some assurance that somebody somewhere has done the deep thinking on the underlying integrity of the design. That's what I trust the full paper supplies. Neverthless, if you have suggestions towards improving its readability, please let me know via email, [Redmine](https://xalgorithms.redminepro.net) or [GitLab](https://gitlab.com/xalgorithms-alliance).

I started off mentioning that "a little while ago" I decided to solve a contradiction. That was in 1989, and I was 31! At the time I was aware that this would be a career-long project, so I'd have to pace myself. After pushing this rock up a hill for three decades, suddenly now I find myself 31 twice over, running as fast as I can to keep up, as the darned thing bounds along down the other side at an increasing pace, picking up all sorts of contributions from a rapidly growing community of very thoughtful people!

Help!

For those of you so inclined, my second blog entry will outline how you can.
