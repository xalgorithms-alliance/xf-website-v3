import { Button } from 'xf-material-components/package/index'
import Persona from './Persona'
import style from './Build.module.css'

export default function Build() {
    return(
        <div className={style.buildhold}>
            <div className={style.dualholdc}>
                <div className={style.dualmod}>
                    <h4>Individuals</h4>
                    <h2>Do Your Best Work Here.</h2>
                    <p>We seek contributors who face complexity, are motivated to discover intuitive solutions, and pursue mutual understanding, and advancement. </p>
                </div>
                <div className={style.hline}/>
                <div className={style.dualmod}>
                    <h4>Institutions</h4>
                    <h2>Build on Your Strengths</h2>
                    <p>We need institutions to substantively review our work, to provide R&amp;D funding, and  to experiment with implementation.</p>
                </div>
               
            </div>
            <div className={style.personagrid}>
                <Persona title="Dev" caption="It’s tables all the way down." cta="Source Code" target="https://gitlab.com/xalgorithms-alliance" image="/dev.png"/>
                <Persona title="Brand Guidelines" caption="Camaraderie writ large." cta="Comms Docs" image="/comm.png" target='https://communications.xalgorithms.org/'/>
                <Persona title="Research" caption="Learning through building." cta="GitLab" image="/Research.png" target="https://gitlab.com/xalgorithms-alliance/data-with-direction-specification/dwds-documents/-/tree/master/current?ref_type=heads"/>
                <Persona title="Advisors" caption="Great wisdom is generous." cta="Get in Touch" image="/Advisor.png" target="mailto:jpotvin@xalgorithms.org"/>
                <Persona title="Donors" caption="Motivation Expressed with Money." cta="Get in Touch" image="/Donor.png" target="mailto:jpotvin@xalgorithms.org"/>

                </div>
        </div>
    )
}