import { useState, useEffect } from 'react';

export default function Carousel({ items, interval = 3000 }) {
  const [currentIndex, setCurrentIndex] = useState(1); // Start at 1, for the added clone

  // Check if items are images (URLs as strings) or components
  const isImageCarousel = typeof items[0] === 'string';
  const slides = [items[items.length - 1], ...items, items[0]]; // Add first and last clones for looping

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentIndex((prevIndex) => {
        const nextIndex = prevIndex + 1;
        // Check if we've reached the end of the slides
        if (nextIndex === slides.length - 1) {
          return 1; // Jump back to the real first slide
        }
        return nextIndex; // Move to the next slide
      });
    }, interval);

    return () => clearInterval(intervalId);
  }, [interval, slides.length]);

  // Handle backward transition to the last real slide
  useEffect(() => {
    if (currentIndex === 0) {
      setCurrentIndex(slides.length - 2);
    }
  }, [currentIndex, slides.length]);

  return (
    <div className="carousel-container">
      <div className="carousel-slides">
        {slides.map((item, index) => (
          <div
            key={index}
            className="carousel-slide"
            style={{ display: index === currentIndex ? 'block' : 'none' }} // Only show current slide
          >
            {isImageCarousel ? (
              <img src={item} alt={`Slide ${index}`} className="carousel-image" />
            ) : (
              <div className="carousel-component-wrapper">{item}</div>
            )}
          </div>
        ))}
      </div>

      <style jsx>{`
        .carousel-container {
          position: relative;
          width: 100%;
          height: 100%;
          overflow: hidden;
        }
        .carousel-slides {
          display: flex;
          width: 100%;
          height: 100%;
        }
        .carousel-slide {
          width: 100%;
          height: 100%;
        }
        .carousel-image {
          width: 100%;
          height: 100%;
          object-fit: cover;
        }
        .carousel-component-wrapper {
          width: 100%;
          height: 100%;
          display: flex;
          align-items: center;
          justify-content: center;
        }
      `}</style>
    </div>
  );
}
