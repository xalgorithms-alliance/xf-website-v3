import React, { useState, useEffect } from 'react';

const ImageFullScreen = ({ src, alt, width = '200px', height = 'auto' }) => {
  const [isFullScreen, setIsFullScreen] = useState(false);
  const zoomLevel = 1; // Set the desired zoom level

  const toggleFullScreen = () => {
    setIsFullScreen(!isFullScreen);
  };

  // Effect to toggle body scroll
  useEffect(() => {
    if (isFullScreen) {
      document.body.style.overflow = 'hidden'; // Prevent body scroll
    } else {
      document.body.style.overflow = ''; // Restore body scroll
    }

    // Cleanup function to reset overflow style
    return () => {
      document.body.style.overflow = '';
    };
  }, [isFullScreen]);

  return (
    <div>
      <img
        src={src}
        alt={alt}
        onClick={toggleFullScreen}
        style={{
          width: width,
          height: height,
          objectFit: 'contain',
          cursor: 'pointer',
          transition: 'transform 0.3s ease',
        }}
      />
      {isFullScreen && (
        <div
          onClick={toggleFullScreen} // Close fullscreen on overlay click
          style={{
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(0, 0, 0, 0.9)',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            zIndex: 1000,
            overflow: 'auto',
            cursor: 'pointer', // Indicate clickable overlay
          }}
        >
          <img
            src={src}
            alt={alt}
            onClick={(e) => e.stopPropagation()} // Prevent click event propagation to overlay
            style={{
              transform: `scale(${zoomLevel})`,
              transition: 'transform 0.3s ease',
              maxWidth: '90vw', // Constrain width within the viewport
              maxHeight: '90vh', // Constrain height within the viewport
              objectFit: 'contain',
              pointerEvents: 'auto',
              display: 'block',
            }}
          />
          <button
            onClick={toggleFullScreen}
            style={{
              position: 'absolute',
              top: '20px',
              right: '20px',
              backgroundColor: '#ffffff',
              border: 'none',
              padding: '10px 15px',
              fontSize: '18px',
              cursor: 'pointer',
            }}
          >
            Close
          </button>
        </div>
      )}
    </div>
  );
};

export default ImageFullScreen;
