import style from './NavGetInvolved.module.css'

export default function NavDeveloper() {
    return(
        <div className={style.triflex}>
            <div>
                <div style={{backgroundImage: 'url("/writing-images/brand-process-hero.jpg")', width: '360px', backgroundSize: 'cover', backgroundPosition: 'center', marginBottom: '1em', height: '200px' }} />
                <p>
                It's tables all the way down.
                </p>
            </div>
            <div className={style.hline}/>
            <div className={style.flexdownb}>
                <a className={style.a} id={style.nomargin} href="https://gitlab.com/xalgorithms-alliance/" target="_blank">Gitlab</a>
                <div className={style.hrule}/>
                <p className="label" id={style.yesmargin}>Reference Implementations</p>
                <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/rule-authoring-software/xalgo-devrm-v3/-/commits/dev" target="_blank">Rule Maker</a>
                <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/rule-networking-software/prod-impl/-/blob/master/README.md" target="_blank">Rule Reserve</a>
                <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/rule-networking-software/prod-impl/-/blob/master/README.md" target="_blank">Rule Taker</a>
                <div className={style.hrule}/>
                <p className="label" id={style.yesmargin}>Sequence Diagrams</p>
                <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/dwds-documents/-/tree/master/rm-sequence-diagram" target="_blank">Rule Maker</a>
                <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/dwds-documents/-/tree/master/rr-sequence-diagram" target="_blank">Rule Reserve</a>
                <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/dwds-documents/-/tree/master/rt-sequence-diagram" target="_blank">Rule Taker</a>
            </div>
        </div>
    )
}