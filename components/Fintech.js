import style from './Fintech.module.css';
import Image from 'next/image';
import Link from 'next/link';
import { Button } from 'xf-material-components/package/index';

export default function Award() {
    return (
        <div>
            
            <div>
                
                <h4>
                    MEXICO CITY – November 13, 2024 – Xalgorithms Foundation has been named 'Finnovator for 2024'
                    by Central Banking Publications
                </h4>
                <br />
                <p>
                    At the prestigious FinTech & RegTech Global Awards ceremony, the
                    award was presented during the Central Banking Autumn Meetings in Mexico City.
                    <br />
                    <br />
                    "Xalgorithms Foundation is the first not-for-profit, free and open-source software organization to receive
                    the Finnovator award," said Chris Jeffrey, editor-in-chief of Central Banking. "Their Internet of Rules
                    framework has the potential to simplify the management of complex rules systems and vast amounts of
                    data for regulators, global trade and governance institutions, ultimately enhancing their oversight,
                    enforcement, and policy-making capabilities."
                    <br />
                    <br />
                    This recognition highlights the original work of Xalgorithms and its contributors
                    in developing the Data With Direction Specification (DWDS)—a tabular approach
                    to ‘rules-as-data’ that makes normative information in any language (statements
                    of MUST, MAY, and SHOULD, along with their negatives and synonyms) more
                    coherent, maintainable, accessible, and usable at any scale. Across sectors and
                    platforms, the DWDS framework supports both human comprehension as well as
                    fast and efficient machine automation, when there is stakeholder permission.
                    <br />
                    <br />
                    Joasia E. Popowicz, associate editor of Central Banking, said: “The processing of vast amounts of data
                    into actionable insights is important in regulation as well as trade, which plays an important role in the
                    global economy and the economic environment in which central banks make their decisions.
                    Xalgorithms’ offers a unique solution.”
                    <br />
                    “We are truly honoured. Our effort has been to enable anyone to link awareness of ‘what is’ with
                    awareness of ‘what ought to be’," said Joseph Potvin, Executive Director and Lead Systems Architect at
                    Xalgorithms Foundation. “Under free/libre licensing we offer a method for anyone to author, publish,
                    discover, fetch, scrutinize, prioritize, and with stakeholder authorization, automate 'rules-as-data'.
                </p>
            </div>
            <br />
            <div className={style.buildhold}>
                <div className={style.grid_25_75}>
                    <div>
                        <Image
                            src="/fintech/trophy.jpg"
                            layout="intrinsic"
                            width={250}
                            height={350}
                            alt="Fintech Trophy"
                        />
                    </div>
                    <div />
                    <div>
                        <h4>
                            Joseph Potvin and Nhamo Mtetwa, jointly accepted the award at the ceremony amidst the Central
                            Banking Autumn Meetings in Mexico City.
                        </h4>
                        <br />
                        <p className={style.textLeft}>
                        Joseph Potvin and Nhamo Mtetwa, Data Science Research Advisor to Xalgorithms
                            Foundation, jointly accepted the award at the ceremony amidst the Central
                            Banking Autumn Meetings in Mexico City. Mtetwa explained: “As deployment proceeds internationally, we hope there will
                            emerge a true 'Internet of Rules' worthy of the name – a paradigm where
                            independent, self-contained rules-as-data are discoverable and transmissible
                            efficiently and flexibly from any source repositories in which they are maintained,
                            to any applications that use them.”
                            Central bankers are rule-makers—establishing regulations for monetary policy, financial stability, and
                            payments; and they are also rule-takers—having to operate within mandates, legislation, and
                            international agreements. Xalgorithms’ DWDS framework addresses the challenges facing both roles by
                            making rules simpler, more comprehensible, and actionable.
                        </p>
                        <br />
                        <ul className={style.textLeft}>
                            <li>
                                <strong>RuleMaker Web App</strong> (Apache 2.0 license): A feature-rich RuleMaker Web app can be used to express and publish any type of rule as a simple, auditable data structure, including rule metadata, logic, reference data, individually or in a chain of rules, and with a practical method for managing uncertainty.
                            </li>
                            <li>
                                <strong>RuleReserve Network Service</strong> (AGPL 3.0 license): The RuleReserve network service provides a flexible way to store, sift, and transmit rules-as-data on public and/or restricted nodes.
                            </li>
                            <li>
                                <strong>RuleTaker Auxiliary Component</strong> (Apache 2.0 license): A lightweight RuleTaker auxiliary component is designed to be adapted to any other system, for end-users to immediately discover, fetch, organize, scrutinize, and use rules that are deemed to be 'in effect', 'applicable', and 'invoked'.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className={style.buildhold}>
                <Image
                    src="/fintech/awardSpeech.jpg"
                    layout="intrinsic"
                    width={800}
                    height={500}
                    alt="speech info"
                />
            </div>
            <div className={style.buildhold}>
                <div className={style.grid_75_25}>
                    <div className={style.textLeft}>
                        <p>
                            Potvin elaborated: “The network-based
                            system our team has designed uses simple
                            tabular data structures for efficient
                            expression and storage, and extremely fast
                            discovery and transmission of rules. Any
                            rules deemed to be ‘in effect’ for a
                            jurisdiction, and ‘applicable’ to a sector,
                            and ‘invoked’ by a circumstance, can be
                            almost instantaneously delivered in any
                            natural language to humans, usable by any
                            computational platform for processing.”
                            <br />
                            <br />
                            The Data With Direction Specification (DWDS) was the subject of Potvin’s
                            doctoral thesis at Université du Québec (2023). He holds a master's degree from
                            Cambridge University and an undergraduate degree from McGill University.
                            <br />
                            <br />
                            He added: “We express our gratitude to the judges convened by Central Banking
                            Publications for this honour, and share with them the pursuit of a more
                            integrated, efficient, and fair international monetary and financial system.”
                            <br />
                            <br />
                            <a href="/fintech/mediaRelease.pdf" download="Xalgorithms-Finnovator2024-Award_MediaRelease_2024-11-29.pdf">
                                Download Media Release (PDF)
                            </a>
                            <br />
                            <br />
                            <a href="/fintech/pressRelease.pdf" download="Press release_ Seventh annual Central Banking FinTech & RegTech Global Awards_2024-11-13.pdf">
                                Download Seventh Annual Central Banking FinTech and RegTech Global Awards Announcement (PDF)
                            </a>
                            <br />
                            <br />
                        </p>
                        <br />
                    </div>
                    <div />
                    <div>
                        <Image
                            src="/fintech/awardHold.jpg"
                            alt="Fintech Trophy"
                            layout="responsive"
                            width={650}
                            height={750}
                            objectFit="contain"
                            priority
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}
