export default function TimelineItem({img, title, date, content}) {
    return(
        <div>
            <img src={img}/>
            <h5>{title}</h5>
            <p>{date}</p>
            <p>{content}</p>
        </div>
    )
}