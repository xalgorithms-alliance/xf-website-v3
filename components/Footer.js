import style from './Footer.module.css'
import EmailSignup from './emailSignup'
import Link from 'next/link'
import Build from './Build'

export default function Footer() {
    return (
        <>
       
        <div className={style.footerbg}>
        <Build />
            <div className={style.colorbg}>
            <div className={style.emailhold}>
                <div>
                    <div className={style.ctaholding}>
                        <h4>Want Updates?</h4>
                        <br/>
                        <p className="smallT">
                        Follow @Xalgorithms on X to get updates on the Xalgorithms Foundation, our projects, writings, and ideas. 
                        </p>
                        {/*
                        <br/>
                        <InputText placeholder="Sign up for updates"/>
                        */}

                        <a href="https://x.com/xalgorithms" target="_blank">
                        <button className={style.altbutton}> Follow Xalgorithms on X</button>
                        </a>
                    </div>
                </div>
            </div>
            </div>
        </div>
            <div className={style.footerhold}>
                <div>
                    <p>
                    Text Graphics CC-by International 4.0  
                    <br />
                    XRM, XRS, XRT Apache 2.0 
                    <br />
                    XRR Affero GPL 3.0
                    </p>
                </div>
                <div className={style.linkgridvar}>
                    <div>
                        <a href="https://gitlab.com/xalgorithms-alliance/xalgo-devrm-v2" target="_blank" className={style.footerlink}>
                            XRM dev
                        </a>
                    </div>
                    <div>
                        <a href="https://development.xalgorithms.org/" target="_blank" className={style.footerlink}>
                            Developer Docs
                        </a>
                        <a href="https://gitlab.com/xalgorithms-alliance" target="_blank" className={style.footerlink}>
                            Source Code
                        </a>
                        <a href="https://communications.xalgorithms.org/" target="_blnk" className={style.footerlink}>
                            Design and Brand
                        </a>
                    </div>
                    <div>
                        <a href="https://twitter.com/Xalgorithms" target="_blank" className={style.footerlink}>
                            Twitter
                        </a>
                        <a href=" https://www.linkedin.com/company/xalgorithms/" target="_blank" className={style.footerlink}> 
                            LinkedIn
                        </a>
                        <a href="mailto:jpotvin@xalgorithms.org " target="_blank" className={style.footerlink}>
                            Email
                        </a>
                    </div>
                    <div>
                        <Link href={'/donate'}>
                            <a className={style.footerlink}>
                            Donate
                            </a>
                        </Link>
                        <a href="https://wordpress-309334-1044070.cloudwaysapps.com/" target="_blank" className={style.footerlink}>
                            Archive
                        </a>
                    </div>
                </div>
            </div>
            </>
    )
}