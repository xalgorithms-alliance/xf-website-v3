import React from 'react';
import style  from './Navbar.module.css'
import LogoAnimate from './LogoAnimate';
import Link from 'next/link'
import DropDown from './DropDown';
import Image from 'next/image'
/* submenu modules*/
import NavGetInvolved from './NavGetInvolved';
import NavDeveloper from './NavDeveloper';
import NavAbout from './NavAbout';
import NavUsecase from './NavUsecase';
import NavWriting from './NavWriting';

export default function NavBar({ select, setLang, options = [] }) {

  const [isOpen, setIsOpen] = React.useState(false);
  const renderOptions = () => {
    return options.map(({ value, label }, index) => (
      <option value={value} key={index}>
        {label}
      </option>
    ));
  };
  

    return (
      <>
      <div>
        <div className={style.navBar}>
          <div className={style.internalMargin}>
            <LogoAnimate />
            <div> 
                <Link href="/fintech-award">
                  <a>
                    <div className={style.awardHold}>
                      <Image src="/FinnovatorAward.png" layout='fill'/>
                    </div>
                  </a>
                </Link>
              </div>
            <div className={style.directory}>
              <div className={style.navBox}>
                {/*}
                  <Link href="/uses">
                    <a className={style.linkshow}>
                      Uses
                      <span className={style.linkunderline}/>
                    </a>
                  </Link>
    */}
                  {/* <Link href="/concepts">Concepts</Link> */}


                    {/*Get Involved subnav--------------*/}

                  <div className={style.optionHold} id={style.involve}>
          
                      <a className={style.linkshow}>
                        Get Involved
                        <span className={style.linkunderline}/>
                      </a>
                   
                    <div className={style.submenuHold}>
                      <NavGetInvolved/>
                    </div>
                  </div>

                  {/*Developer subnav--------------*/}

                  <div className={style.optionHold} id={style.dev}>
                    
                      <a className={style.linkshow}>
                        Developer
                        <span className={style.linkunderline}/>
                      </a>
                   
                    <div className={style.submenuHold}>
                      <NavDeveloper/>
                    </div>
                  </div>

                  {/*About subnav--------------*/}

                  <div className={style.optionHold} id={style.org}>
  
                      <a className={style.linkshow}>
                        About
                        <span className={style.linkunderline}/>
                      </a>
                      
                  
                    <div className={style.submenuHold}>
                      <NavAbout/>
                    </div>
                  </div>

                   {/*Usecases subnav--------------*/}

                  <div className={style.optionHold} id={style.use}>
                      <a className={style.linkshow}>
                        Working Groups
                        <span className={style.linkunderline}/>
                      </a>
                    <div className={style.submenuHold}>
                      <NavUsecase/>
                    </div>
                  </div>

                  {/*Writing subnav--------------*/}

                  <div className={style.optionHold} id={style.write}>
                      <a className={style.linkshow}>
                        Reflections
                        <span className={style.linkunderline}/>
                      </a>
                    <div className={style.submenuHold}>
                      <NavWriting/>
                    </div>
                  </div>
                  {/* <a href="https://development.xalgorithms.org" target="_blank">Developer</a> */}
              </div>
          </div>
          {/*
          <div id="languageBox">
              <select onChange={select} value={setLang} className={style.dropdown}>
                {renderOptions()}
              </select>
          </div>
          */}
        </div>
        <div className={style.mobileNavBar}>
          <div className="pad-box">
          <div className={style.holdFlex}>
          <LogoAnimate />
          <div> 
                <Link href="/fintech-award">
                  <a>
                    <div className={style.awardHold}>
                      <Image src="/FinnovatorAward.png" layout='fill'/>
                    </div>
                  </a>
                </Link>
              </div>
            
            </div>
            </div>
            <div style={{zIndex: '7'}}>
              { !isOpen ? (
              <button className={style.hamburgerHold} onClick={() => setIsOpen(true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="26.5" height="17" viewBox="0 0 26.5 24">
                  <title>hamburgermenu</title>
                  <rect width="26.5" height="1" fill="#000"/>
                  <rect y="8" width="26.5" height="1" fill="#000"/>
                  <rect y="16" width="26.5" height="1" fill="#000"/>
                </svg>
              </button>
              ) : (
                <div style={{zIndex: '7', position: 'relative'}}>
                  <button className={style.hamburgerHold} onClick={() => setIsOpen(false)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16.71" height="16.71" viewBox="0 0 16.71 16.71">
                      <title>exout</title>
                      <rect x="-2.81" y="8" width="22.63" height="1" transform="translate(-3.67 8.35) rotate(-45)" fill="#000"/>
                      <rect x="8" y="-2.81" width="1" height="22.63" transform="translate(-3.67 8.35) rotate(-45)" fill="#000"/>
                    </svg>
                  </button>
                </div>
              )
              }
              <div className={isOpen ? (style.displayMenu) : (style.hideMenu)}>
              {/*
              <div style={{marginTop: '60px'}}>
                <Link href="/uses"><a className={style.mobilelinkshow}>Uses</a></Link>
              </div>
              */}
              {/*
              <div>
                <Link href="/concepts">Concepts</Link>
              </div>
              */}
              <DropDown title="Get Involved"><NavGetInvolved/></DropDown>
              <DropDown title="Developer"><NavDeveloper/></DropDown>
              <DropDown title="About"><NavAbout/></DropDown>
              <DropDown title="Use Cases"><NavUsecase/></DropDown>
              <DropDown title="Writing"><NavWriting/></DropDown>
        </div>
            </div>
            </div>
        </div>
      </div>
       
      </>
    )
  }