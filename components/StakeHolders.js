import style from './RuleInfo.module.css'
import { Button } from 'xf-material-components/package/index'

export default function StakeHolders() {
    return (
        <div>
            <div className={style.buildhold}>
                <div>
                    <h2>Xalgorithms Foundation Stakeholder Roles</h2><br />
                    <h3> Working Assumptions About their Market Revenues and Costs </h3><br />
                    <h4> Xalgorithms Foundation </h4><br />
                </div>
            </div>

            <div className={style.buildhold}>
                <h2>Xalgorithms Foundation Stakeholder Roles and Working Assumptions About their Market Revenues and Costs</h2><br />
            </div>

            <div className={style.triplehold}>

                <div className={style.dualmodc}>
                    <h3>Roles</h3><br />
                    <span className={style.textLeft}>
                        <ul className={style.parastyle}>
                            <li>Maintain the DWDS specification (licensed CC-by 4.0);</li>
                            <li>Maintain three software reference implementations of RuleMaker and RuleTaker software (licensed Apache 2.0) and of RuleReserve (licensed AGPL);</li>
                            <li>Maintain technical documentation for each reference implementation (licensed CC-by 4.0);</li>
                            <li>Provide organizational support for the Xalgorithms Alliance;</li>
                            <li>Provide quarterly briefings and annual reports.</li>
                        </ul>
                    </span>
                </div>

                <div className={style.hline}></div>

                <div className={style.dualmodc}>
                    <h3>Revenue Basis (Design)</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                        <p>A market-based not-for-profit organization resourced through grants and contributions in 5 categories:</p>
                        <ul>
                            <li>
                                <strong>Strategic Implementers:</strong> Make a CAD$100,000 annual contribution, along with an equivalent in-kind commitment to joint research and development. 
                                <br />Market Rationale: Generate revenues or cost savings for themselves based on the free and open-source DWDS system.
                            </li>
                            <li>
                                <strong>Experimenting Implementers:</strong> Make a CAD$10,000 annual contribution, along with an equivalent in-kind commitment to joint research and development. 
                                <br />Market Rationale: Experiment with generating revenues or cost savings for themselves based on the free and open-source DWDS system.
                            </li>
                            <li>
                                <strong>Developers & Researchers:</strong> Make a CAD$500 annual contribution towards member services that directly or indirectly assist the participatory nature of Xalgorithms Alliance research and development. 
                                <br />Market Rationale: Each learns from others by participating in Xalgorithms community R&D.
                            </li>
                            <li>
                                <strong>R&D Donors:</strong> Provide grants and contributions, in cash and/or in-kind to support joint research and development, either to Xalgorithms Foundation or to any of the active Working Groups. 
                                <br />Market Rationale: Advance DWDS for its improvements to the market as a whole, or to a use case sector that benefits the donor or aligns with their preferences.
                            </li>
                            <li>
                                <strong>Board Members and External Advisors:</strong> Provide in-kind consultative assistance towards the development and deployment of an Internet of Rules. 
                                <br />Market Rationale: Advance DWDS for its improvements to the market as a whole, or to a specific industry use case, which benefits the donor or aligns with their preferences.
                            </li>
                        </ul>
                    </span>
                </div>

                <div className={style.hline}></div>

                <div className={style.dualmodc}>
                    <h3>Costs of Operation (Design)</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                        <ul>
                            <li>Staffing and operational overhead;</li>
                            <li>Documentation costs;</li>
                            <li>Event costs.</li>
                        </ul>
                    </span>
                </div>

            </div>

            <div className={style.buildhold}>
                <h2>Systems Integration Firms</h2><br />
                <div className={style.triplehold}>
                <div className={style.dualmodc}>
                    <h3>Roles</h3><br />
                    <span className={style.textLeft}>
                        <li className={`${style.parastyle}`}>
                            Plan, arrange and market corporate subscriptions for the firm’s own differentiated value-added packages of products and services (including training) extended from a branded DWDS distro.
                        </li>
                    </span>
                </div>
                <div className={style.hline}></div>
                <div className={style.dualmodc}>
                    <h3>Revenue Basis (Assumption):</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                        <li >
                        Subscription fees for packages of products and services (including training).
                        </li>
                        <li>
                        Ad hoc consulting fees (e.g. for customization; integration; tailored support).
                        </li>
                    </span>
                </div>
                <div className={style.hline}></div>
                <div className={style.dualmodc}>
                    <h3>Costs of Operation (Assumption):</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                    <ul>
                    <li>Staffing and operational overhead;</li>
                    <li>Contractors;</li>
                    <li>Xalgorithms Alliance fees enable ongoing maintenance of the DWDS specification and reference implementations.</li>
                    <li>Potentially, Working Group fees to advance a use-case domain of DWDS implementation.</li>
                </ul>

                    </span>
                </div>
            </div>
            </div>

            <div className={style.buildhold}>
                <h2>Contractors</h2><br />
                <div className={style.triplehold}>
                <div className={style.dualmodc}>
                    <h3>Roles</h3><br />
                    <span className={style.textLeft}>
                        <ul className={style.parastyle}>
                            <li>Provide specialized services to:</li>
                            <ul>
                                <li>Xalgorithms Foundation;</li>
                                <li>Assurance firms;</li>
                                <li>Systems integration firms;</li>
                            </ul>
                        </ul>
                    </span>
                </div>
                
                <div className={style.hline}></div>
                
                <div className={style.dualmodc}>
                    <h3>Revenue Basis (Assumption):</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                        <ul>
                            <li>Fee-for-service;</li>
                        </ul>
                    </span>
                </div>
                
                <div className={style.hline}></div>
                
                <div className={style.dualmodc}>
                    <h3>Costs of Operation (Assumption):</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                        <ul>
                            <li>Staffing and operational overhead;</li>
                            <li>Xalgorithms Alliance fees enable ongoing maintenance of the DWDS specification and reference implementations;</li>
                            <li>Potentially, Working Group fees to advance a use-case domain of DWDS implementation.</li>
                        </ul>
                    </span>
                </div>
            </div>
            </div>
            
            <div className={style.buildhold}>
                <h2>Commercial Training Provider</h2><br />
                <div className={style.triplehold}>
                <div className={style.dualmodc}>
                    <h3>Roles</h3><br />
                    <span className={style.textLeft}>
                        <ul className={style.parastyle}>
                            <li>Develop, market and deliver:</li>
                            <ul>
                                <li>training ‘packages’;</li>
                                <li>train-the-trainer ‘packages’;</li>
                                <li>proficiency certifications.</li>
                            </ul>
                            <li>Prepare and sell learning aids:</li>
                            <ul>
                                <li>Written;</li>
                                <li>Presentation;</li>
                                <li>Podcast;</li>
                                <li>Video;</li>
                            </ul>
                            <li>Registry of training services and trainers;</li>
                            <li>Scheduling of training services;</li>
                            <li>Monitor feedback and outcomes.</li>
                        </ul>
                    </span>
                </div>
                
                <div className={style.hline}></div>
                
                <div className={style.dualmodc}>
                    <h3>Revenue Basis (Assumption):</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                        <ul>
                            <li>Fees for courses and workshops;</li>
                            <li>Fees for training aids;</li>
                            <li>Certification exam fees.</li>
                        </ul>
                    </span>
                </div>
                
                <div className={style.hline}></div>
                
                <div className={style.dualmodc}>
                    <h3>Costs:</h3><br />
                    <span className={`${style.parastyle} ${style.textLeft}`}>
                        <ul>
                            <li>Staffing and operational overhead;</li>
                            <li>Subscription fees to the Assurance Firm;</li>
                            <li>Production of training curricula and materials;</li>
                            <li>Training infrastructure (physical; virtual);</li>
                            <li>Marketing and business development relating to its DWDS-based products and services;</li>
                            <li>Xalgorithms Alliance fees enable ongoing maintenance of the DWDS specification and reference implementations;</li>
                            <li>Potentially, Working Group fees to advance a use-case domain of DWDS implementation.</li>
                        </ul>
                    </span>
                </div>
            </div>
            </div>
            <div className={style.buildhold}>
                <h2>'Rules-as-Data' Assured Solution Providers</h2><br />
                <div className={style.triplehold}>
                    
                    <div className={style.dualmodc}>
                        <h3>Roles</h3><br />
                        <span className={style.textLeft}>
                            <ul className={style.parastyle}>
                                <li>Manage independent reviews of DWDS reference implementations:</li>
                                <ul>
                                    <li>Risks of potential patent, copyright and industrial design infringement;</li>
                                    <li>Assurance and failure response methodology and performance regarding system integrity (prevention/detection of tampering), privacy, confidentiality, auditability, reliability (preventing 'bugs' in code, design and architecture), trustworthiness, and authorization controls;</li>
                                    <li>Reference implementation performance speed and efficiency;</li>
                                    <li>System simplicity and elegance.</li>
                                </ul>
                                <li>Produce branded DWDS reference implementations (their own ‘distros’);</li>
                                <li>Maintain errors & omissions insurance.</li>
                            </ul>
                        </span>
                    </div>
                    
                    <div className={style.hline}></div>
                    
                    <div className={style.dualmodc}>
                        <h3>Revenue Basis (Assumption):</h3><br />
                        <span className={`${style.parastyle} ${style.textLeft}`}>
                            <ul>
                                <li>Subscription fees for their branded DWDS reference implementations (‘distros’);</li>
                                <li>Optionally, a service package.</li>
                            </ul>
                        </span>
                    </div>
                    
                    <div className={style.hline}></div>
                    
                    <div className={style.dualmodc}>
                        <h3>Costs of Operation (Assumption):</h3><br />
                        <span className={`${style.parastyle} ${style.textLeft}`}>
                            <ul>
                                <li>Staffing and operational overhead;</li>
                                <li>Errors & omissions insurance;</li>
                                <li>Xalgorithms Alliance fees enable ongoing maintenance of the DWDS specification and reference implementations;</li>
                                <li>Potentially, Working Group fees to advance a use-case domain of DWDS implementation.</li>
                            </ul>
                        </span>
                    </div>

                </div>
            </div>

            
            <div className={style.buildhold}>
                <h2>End User Entities (Client Organizations)</h2><br />
                <div className={style.triplehold}>
                    <div className={style.dualmodc}>
                        <h3>Roles</h3><br />
                        <span className={style.textLeft}>
                            <ul className={style.parastyle}>
                                <li>Implement DWDS solutions in operations;</li>
                                <li>Provide useful feedback.</li>
                            </ul>
                        </span>
                    </div>
                    
                    <div className={style.hline}></div>
                    
                    <div className={style.dualmodc}>
                        <h3>Revenue Basis (Assumption):</h3><br />
                        <span className={`${style.parastyle} ${style.textLeft}`}>
                            <ul>
                                <li>Improved capability, efficiency, and rules conformance.</li>
                            </ul>
                        </span>
                    </div>
                    
                    <div className={style.hline}></div>
                    
                    <div className={style.dualmodc}>
                        <h3>Costs:</h3><br />
                        <span className={`${style.parastyle} ${style.textLeft}`}>
                            <ul>
                                <li>Subscription fees to branded DWDS ‘distros’;</li>
                                <li>Implementation expenses;</li>
                                <li>Staff training;</li>
                                <li>Staff operations;</li>
                                <li>Xalgorithms Alliance fees enable ongoing maintenance of the DWDS specification and reference implementations;</li>
                                <li>Potentially, Working Group fees to advance a use-case domain of DWDS implementation.</li>
                            </ul>
                        </span>
                    </div>
                </div>
                <div>
                    <p className={`${style.parastyle}`}> All of the Working Groups donate an annually-negotiated common % of their revenue to Xalgorithms Foundation.
                        The amount takes into consideration the overall value provided by foundation, the specification and the reference implementations,
                        as well as the foundation's cost of operation, and for context, the income that participating entities are leveraging from the availability of DWDS. 
                        This 'demand-driven' approach will tend towards a minimalist approach to the foundation, the specification and the reference implementations, 
                        which is intentional.
                    </p>
                </div>
               
            </div>
            <div className={style.buildhold}>
                <h2>Free/Open Voluntary Contributors</h2><br />
                <div className={style.triplehold}>
                    <div className={style.dualmodc}>
                        <h3>Roles</h3><br />
                        <span className={style.textLeft}>
                            <ul className={style.parastyle}>
                                <li>Contribute quality work to DWDS specification and reference implementation development;</li>
                                <li>(Volunteers may also receive financial income via other stakeholder roles.)</li>
                            </ul>
                        </span>
                    </div>
                    
                    <div className={style.hline}></div>
                    
                    <div className={style.dualmodc}>
                        <h3>Revenue Basis (Assumption):</h3><br />
                        <span className={`${style.parastyle} ${style.textLeft}`}>
                            <ul>
                                <li>In-kind benefits <a href="https://xalgorithms.org/organization#value">https://xalgorithms.org/organization#value</a></li>
                                <li>Reputation and goodwill that improves financial earning potential via other stakeholder roles;</li>
                                <li>Gain knowhow that improves one’s earning potential generally.</li>
                            </ul>
                        </span>
                    </div>
                    
                    <div className={style.hline}></div>
                    
                    <div className={style.dualmodc}>
                        <h3>Costs:</h3><br />
                        <span className={`${style.parastyle} ${style.textLeft}`}>
                            <ul>
                                <li>Time and effort</li>
                            </ul>
                        </span>
                    </div>
                </div>
            </div>

            <div className={style.buildhold}>

                <h2>Dispute Resolution</h2>
            <div className={style.dualholdc}>
                <div className={style.dualmod}>
                    <p className={`${style.parastyle} ${style.textLeft}`}> 
                    Any dispute involving two or more Members shall be resolved by negotiations.<br/><br/>
                    If there is a dispute between or amongst Members, a resolution will be proposed to the affected parties by a facilitating Member who is elected by a 3/5ths majority (60%) vote open to all Members who choose to vote. They must be given at least 4 calendar days notice before the vote, and a vote-casting period of at least 1 full calendar day. This process does not impinge on the rights or responsibilities of any Member to pursue legal action.<br/><br/>
                    The elected facilitating Member will suggest that his/her intervention should be considered binding or only advisory (as a basis for further negotiations).<br/><br/>
                    Each Member agrees to refrain from initiating legal action against another Member in relation to Xalgorithms Alliance operations without first obtaining a formal written endorsement of the general merits of the planned legal action from the Xalgorithms Alliance Board of Representatives as follows.</p>
                    
                </div>
                <div className={style.hline}></div>
                <div className={style.dualmod}>
                    <p className={`${style.parastyle} ${style.textLeft}`}> 
                    
                    The Board of Representatives's endorsement of legal action against a Member shall be based upon a vote with at least 7 calendar days prior notice, providing a vote-casting period of at least 3 calendar days, that is open to all current Members, which results in at least 3/5ths majority (60%) of the votes cast endorsing the action. The direct parties to the dispute must recused themselves from this vote.<br/><br/>
                    If a Member initiates legal action against one or more other Members in relation to Xalgorithms Alliance Operations without first obtaining this written endorsement, then the plaintiff's own Xalgorithms Alliance Accession Agreement is immediately and automatically revoked (except for non-disclosure provisions), on the grounds that the spirit and the terms of related documents have thereby been violated.
                    </p>
                </div>
               
            </div>
            </div>

            

        </div>
    )
}
