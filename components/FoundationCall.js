import style from './Build.module.css'
import { Button } from 'xf-material-components/package/index'

export default function FoundationCall() {
    return(
        <div className={style.buildhold}>
            
            <div className={style.singlehold}>
                <div className={style.dualmod}>
                <div><img src="/rerelogo.png/" style={{width: '200px'}} /></div>
                    <p>We are a decentralized community of contributors that are collaborating across disciplines and attitudes to design general purpose systems that work.</p>
                </div>
            </div>
            <div className={style.dualholdc}>
                <div className={style.dualmodc}>
                    <div className={style.flexanimate}>
                        <h1 id={style.extra}>Multi-</h1> 
                        <div>
                        <div id={style.national}>
                            <h1>National</h1>
                        </div>
                        <div id={style.scale}>
                            <h1>Scale</h1>
                        </div>
                        <div id={style.lingual}>
                            <h1>Lingual</h1>
                        </div>
                        <div id={style.temporal}>
                            <h1>Temporal</h1>
                        </div>
                        </div>
                    </div>
                </div>
                <div className={style.hline}/>
                <div className={style.dualmodc} id={style.special}>
                <h1>100%</h1>
                    <h5>Free / Libre / Open</h5>
                </div>
                
            </div>
        </div>
    )
}