import style from "./FullWritingFeat.module.css"
import TimelineItem from "./TimelineItem"

export default function Timeline() {

    var line = () => {
        return(
            <div className={style.linehold}>
                <div className={style.hline}/>
                <div className={style.round}/>
                <div className={style.hline}/>
            </div>
        )
    }

    return(
        <div className={style.timehold}>
            <div className={style.writinghold} id={style.sticky}>
                <h4>
                    About the Org
                </h4>
                <p className={style.para}>
                In viverra erat phasellus mauris pretium aliquam. Risus nunc justo quis diam dignissim id massa. Sapien feugiat at lectus habitant quis suscipit mi. Magna lacus, at tellus semper nullam. Massa, imperdiet elementum ornare sed eget dictum. Vel ornare duis semper in pretium. Id condimentum tempor integer rutrum vestibulum enim. Vulputate integer sagittis, tempus quam proin leo. Vitae, aliquam at ipsum donec.

                </p>
            </div>
            <div style={{ width: "100%", backgroundSize: "cover", backgroundPosition: "center center"}} >
                <div style={{ height: "72px"}} /> 
                <div className={style.timelinegrid}>
                    <div>
                        <TimelineItem title="test" date="test" img="./era.png" />
                    </div>
                        {line()}
                    <div>

                    </div>
                    <div>
                        
                    </div>
                        {line()}
                    <div>
                        <TimelineItem title="test" date="test" img="./era.png"/>
                    </div>
                    <div>
                        <TimelineItem title="test" date="test" img="./era.png" />
                    </div>
                        {line()}
                    <div>

                    </div>
                    <div>
                        
                    </div>
                        {line()}
                    <div>
                        <TimelineItem title="test" date="test" img="./era.png"/>
                    </div>
                    <div>
                        <TimelineItem title="test" date="test" img="./era.png" />
                    </div>
                        {line()}
                    <div>

                    </div>
                    <div>
                        
                    </div>
                        {line()}
                    <div>
                        <TimelineItem title="test" date="test" img="./era.png"/>
                    </div>
                </div>
            </div>
        </div>
    )
}