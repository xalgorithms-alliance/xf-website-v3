import style from './NavGetInvolved.module.css'
import Usecase from './Usecas'

export default function NavUsecase() {
    return(
        <div id={style.wide}>
            <p style={{'padding-bottom': "1.5em"}}> Through Working Groups, participants in Xalgorithms Alliance collaborate on practical implementation projects that make use of DWDS free/libre/open source components which enable an Internet of Rules.</p>
            <Usecase title="Earth Reserve Assurance (ERA)" caption="A novel approach to establishing sound money, which can be incrementally integrated into any conventional currency within a multi-currency system, eliminating the need for a central reference unit of account. " cta="Learn More" image="/era.png" target="http://era.xalgorithms.org"/>
            <div style={{height: "1em"}}/>
            <Usecase title="Xalgo4Trade" caption="A network-oriented approach to meet the trade sector's need for a transparent, risk-managed solution that simplifies and semi-automates (or automates) proactive regulatory conformance and supervision among transacting parties." cta="Learn More" image="/trade.png" target="http://trade.xalgorithms.org"/>
            {/*}
            <Usecase title="Economic Petri Dish" caption="Lorem ipsum dolar Lorem ipsum dolar Lorem ipsum dolar Lorem ipsum dolar." cta="Source Code" image="./petri.png"/>
            <Usecase title="Rail Powered Property" caption="Lorem ipsum dolar Lorem ipsum dolar Lorem ipsum dolar Lorem ipsum dolar." cta="Source Code" />
    */}
        </div>
    )
}