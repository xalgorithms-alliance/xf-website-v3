import style from './NavGetInvolved.module.css'
import Link from 'next/dist/client/link'

export default function NavAbout() {
    return(
        <div className={style.triflex}>
            <div>
                <div style={{backgroundImage: 'url("/community.png")', width: '360px', backgroundSize: 'cover', backgroundPosition: 'center', marginBottom: '1em', height: '200px' }} />
                <p>
                A decentralized collaborative organization.
                </p>
            </div>
            <div className={style.hline}/>
            <div className={style.flexdownb}>
                <Link href="/organization"><a className={style.a} id={style.nomargin}>Organization</a></Link>
                <Link href="/community"><a className={style.a}>Community</a></Link>
                <div className={style.hrule}/>
                <p className="label"><br/>Details</p>
                <Link href="/organization#alliance"><a className={style.a}>Xalgorithms Alliance</a></Link>
                <Link href="/organization#value"><a className={style.a}>Free/Libre/Open Value Prop</a></Link>
                <Link href="/organization#open"><a className={style.a}>The Free/Libre/Open Way</a></Link>
                <Link href="https://gitlab.com/xalgorithms-alliance/data-with-direction-specification/stakeholder-roles/-/blob/main/Xalgorithm_Foundation_Stakeholder_Roles_2024-10-17PDF.pdf?ref_type=heads">
                    <a className={style.a} target="_blank" rel="noopener noreferrer">Roles</a>
                </Link>

            </div>
        </div>
    )
}