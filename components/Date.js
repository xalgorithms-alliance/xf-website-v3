import {Icon} from 'xf-material-components/package/index'

export default function Date({title, time}) {
    return(
        <>
        <div style={{display: 'flex', alignItems: 'center', marginBottom: '1em'}}>
            <div style={{padding: '1em', borderRadius: '1em', background: 'var(--primary75)', marginRight: '1em', color: 'var(--mono00)'}}>
                <h3>Thu</h3>
                <p style={{color: 'var(--mono00)'}}>{time}</p>
            </div>
            <div>
                <h4>
                    {title}
                </h4>
                <div style={{display: 'flex', alignItems: 'center', marginBottom: '1em'}}>
                    <a href="https://meet.jit.si/Xalgorithms" target="_blank" style={{textDecoration: 'none'}}>Join Meeting</a>
                    <Icon name="External" fill='var(--primary)'/>
                </div>
            </div>
        </div>
        </>
    )
}