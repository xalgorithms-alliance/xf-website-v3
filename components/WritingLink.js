import Link from 'next/link'
import { Icon } from 'xf-material-components/package/index'
import style from './WritingLink.module.css'

export default function WritingLink({external, label, target}) {
    return(
        external = true ? (
                <a className={style.linkSelector} href={target}>{label}</a>
              
        ) : (
            <Link href={target}>{label}</Link>
        )
    )
}