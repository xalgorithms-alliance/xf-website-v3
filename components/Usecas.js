import style from './Persona.module.css'

export default function Uscase({title, caption, cta, target, image}) {
    return(
        <div className={style.hold}>
            <div className={style.margin}>
                <div style={{height: '150px', width: '150px', backgroundImage: "url(" + image + ")", backgroundSize: "cover", backgroundPosition: "center center"}}></div>
            </div>
            <div>
                <h5 className={style.t}>{title}</h5>
                <p className={style.p}>{caption}</p>
                
                <a href={target} target="_blank" className={style.a}>{cta}  →</a>
            </div>
        </div>
    )
}