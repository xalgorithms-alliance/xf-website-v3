import style from './Header.module.css'
import React from 'react'
import { Button, Grid12, Column } from 'xf-material-components/package/index'

export default function Header({ title, description, buttonLable, target }) {
 return (
     <div className="hero">
         <Grid12>
            <div className={style.headline}>
                <Column>
                    <h2 className="build">
                        {title}
                    </h2>
                    <h4>
                        {description}
                    </h4>
                    <div>
                        <a href={target} target="blank">
                            <Button>
                                {buttonLable}
                            </Button>
                        </a>
                    </div>
                </Column>
            </div>
         </Grid12>
     </div>
 )
}