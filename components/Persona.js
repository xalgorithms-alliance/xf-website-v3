import style from "./Persona.module.css"

export default function Persona({title, caption, cta, target, image, vert}) {
    return(
        <div className={  vert ? (style.hold) : (style.alt)}>
            <div className={style.margin}>
                <img src={image} className={style.imgsz}/>
            </div>
            <div className={style.lheight}>
                <h5 className={style.t}>{title}</h5>
                <p className={style.p}>{caption}</p>
                
                <a href={target} className={style.a} target="_blank">{cta}  →</a>
            </div>
        </div>
    )
}