import style from "./FullWritingFeat.module.css";

export default function UsecaseCall() {
    return(
        <div className={style.featholdb}>
            <div>
                <div style={{backgroundImage: "url(/writing-images/gis.jpg)", width: "100%", backgroundSize: "cover", backgroundPosition: "center center"}} className="picHeight"/>
                <h5 className={style.t}>Earth Reserve Assurance (ERA)</h5>
                <p className={style.p}>A framework improving stability of worth while incentivize long term assurance of commodities with distributed verifiability. </p>
                <a href="http://era.xalgorithms.org" target="_blank" className={style.a}>Learn More  →</a>
            </div>
            <div>
                <div style={{backgroundImage: "url(/writing-images/halifax-port.png)", width: "100%", backgroundSize: "cover", backgroundPosition: "center center"}} className="picHeight"/>
                <h5 className={style.t}>Xalgo4Trade</h5>
                <p className={style.p}>Xalgorithms’ Internet of Rules is a general-purpose, transformative method for 'rules-as-data' to simplify, clarify and automate regulatory conformance and efficiency in cross-border trade.

</p>
                <a href="http://trade.xalgorithms.org" target="_blank" className={style.a}>Learn More  →</a>
            </div>
        </div>
    )
}