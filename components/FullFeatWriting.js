import style from "./FullWritingFeat.module.css"

export default function FullFeatWriting({img, title, summary, target}) {
    return(
        <div className={style.feathold}>
            <div className={style.writinghold}>
                <h4>
                    {title}
                </h4>
                <p className={style.para}>{summary}</p>
                <a className={style.a} href={target}>Read  →</a>
            </div>
            <div style={{backgroundImage: "url(/" + img + ")", width: "100%", backgroundSize: "cover", backgroundPosition: "center center"}} className="picHeight"/>
        </div>
    )
}