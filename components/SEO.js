import Head from 'next/head'
import { useState, useEffect } from 'react'

export default function SEO ({img, title, description, tags=[]}) {
    
    const [tagstring, SetTagstring] = useState("")

    {/*default tags are declared here*/}

    const tagsList = [
        {tag: "Oughtomation"},
        {tag: "Xalgorithms"},
        {tag: "Xalgorithms Alliance"},
        {tag: "Xalgorithms Foundation"},
        {tag: "non-profit"},
        {tag: "Open Source"},
        {tag: "Free Software"},
        {tag: "Libre"},
        {tag: "Xalgo4Trade"},
        {tag: "Working Group"},
        {tag: "Earth Reserve Assurance"},
        {tag: "ERA"},
        {tag: "ERA Framework"},
        {tag: "Earth Reserve Assurance Framework"},
        {tag: "IoR"}, 
        {tag: "Internet of Rules"},
        {tag: "RuleMaker"},
        {tag: "RuleTaker"},
        {tag: "Internet"},
        {tag: "Ontology"},
        {tag: "Semantic Standards"},
        {tag: "Normative Data"},
        {tag: "Data Model"},
        {tag: "Data Standard"},
        {tag: "Standard"},
        {tag: "Logic"},
        {tag: "Internet Engineering"}, 
        {tag: "Rules as Data"},
        {tag: "Digital Rules"},
        {tag: "Computational Law"},
        {tag: "Computational Rules"},
        {tag: "Rules Automation"},
        {tag: "Algorithms"},
        {tag: "Algorithm"},
        {tag: "Algorithmic Law"},
        {tag: "Algorithmic Governance"},
        {tag: "Tabular Programming"},
        {tag: "AI"},
        {tag: "Artificial Intelligence"},
        {tag: "Natural Language Processing"},
        {tag: "NLP"},
        {tag: "Appropriate Technology"},
        {tag: "LegalTech"},
        {tag: "RegTech"},
        {tag: "GovTech"},
        {tag: "TaxTech"},
        {tag: "TradeTech"},
        {tag: "FinTech"},
        {tag: "Digital Government"},
        {tag: "eGov"},
        {tag: "RaC"}, 
        {tag: "Rules as Code"}
    ]

    {/*checks to see if custom tags are passed by a blog post, if not, will concatenate with the const tagList*/}
    
    const renderTags = () => {
        tags[0] ? (
        tags.map(({tag}) => {
            SetTagstring(tagstring => tagstring.concat(tag + ", "))
        }) 
        ) : (
        tagsList.map(({tag}) => {
            SetTagstring(tagstring => tagstring.concat(tag + ", "))
        }) 
        )
    }

    useEffect(() => {
        renderTags() 
    }, []);

    return (
        <Head>
            <title>{title}</title>
            <link rel="icon" href="/favicon.ico" />
            <meta name="description" content={description} />


            {/*Google SEO*/}
            <meta itemprop="name" content={title}/>
            <meta itemprop="description" content={description}/>
            <meta itemprop="image" content={"https://hopeful-blackwell-2d9bad.netlify.app/" + img}/>

            {/*Twitter SEO*/}
            <meta name="twitter:card" content="summary_large_image"/>
            <meta name="twitter:title" content={title}/>
            <meta name="twitter:description" content={description}/>
            <meta name="twitter:image:src" content={"https://hopeful-blackwell-2d9bad.netlify.app/" + img}/>

            {/*Open Graph SEO*/}
            <meta property="og:title" content={title}/>
            <meta property="og:type" content="website"/>
            <meta property="og:url" content=""/>
            <meta property="og:image" content={"https://hopeful-blackwell-2d9bad.netlify.app/" + img}/>
            <meta property="og:image:width" content=""/>
            <meta property="pg:image:height" content=""/>
            <meta property="og:description" content={description}/>
            <meta property="og:site_name" content=""/>
            <meta property="article:tag" content={tagstring}/>

        </Head>
    )
}
