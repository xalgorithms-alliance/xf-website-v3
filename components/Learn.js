import style from './Build.module.css'
import { Button } from 'xf-material-components/package/index'

export default function Learn() {
    return(
<div className={style.buildhold}>
            <h2>We've Been Listening, <br /> Thinking, Building and Refining.</h2>
            <div className={style.singlehold}>
                <div className={style.dualmod}>
                    <p>And now, with a complete end-to-end design, we're excited to share DWDS (Data With Direction Specification), our reference implementations, and use cases in development under Xalgorithms-hosted working groups.</p>
                </div>
               
            </div>
            <div className={style.learngrid}>
                <div className={style.learnmod}>
                    <h5>
                    How We Think
                    </h5>
                    <a href="https://gitlab.com/xalgorithms-alliance/data-with-direction-specification/dwds-documents/-/tree/master/current" target="_blank" className={style.a}>Core Documents →</a>
                </div>
                <div className={style.learnmod}>
                    <h5>
                    What We Build
                    </h5>
                    <a href="https://development.xalgorithms.org/" target="_blank" className={style.a}>Development Docs  →</a>
                </div>
                <div className={style.learnmod}>
                    <h5>
                    How We Communicate
                    </h5>
                    <a href="https://communications.xalgorithms.org/"  target="_blank" className={style.a}>Communication Docs  →</a>
                </div>
                <div className={style.learnmod}>
                    <h5>
                    Everyone's Input
                    </h5>
                    <a href="https://lists.xalgorithms.org/mailman/listinfo/community" target="_blank" className={style.a}>Email Archive  →</a>
                </div>

            </div>
        </div>
    )
}