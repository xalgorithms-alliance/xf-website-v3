import style from "./FullWritingFeat.module.css"
export default function TitleMod({ title, description, img, fixedImageSize = false }) {
    return (
        <div className={style.feathold}>
            <div className={style.writinghold}>
                <h1>{title}</h1>
                {description}
            </div>
            <div
                style={{
                    backgroundImage: `url(/${img})`,
                    width: "100%",
                    height: "auto",
                    aspectRatio: "1", 
                    backgroundSize: fixedImageSize ? "contain" : "cover",
                    backgroundPosition: "center center",
                    backgroundRepeat: fixedImageSize ? "no-repeat" : "initial",
                }}
                className="picHeight"
            />
        </div>
    );
}
