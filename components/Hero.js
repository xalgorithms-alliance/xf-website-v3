import style from './Hero.module.css'
import { InputDropdown, Button } from 'xf-material-components/package/index'
import Link from 'next/dist/client/link'
import ComponentSlider from './ComponentSlider.js';
import Image from 'next/image';

const logic_gate_images = [
    '/logic_gate/LogicGate-1.png',
    '/logic_gate/LogicGate-2.png',
    '/logic_gate/LogicGate-3.png',
    '/logic_gate/LogicGate-4.png',
  ];
const logic_text =
[   
    <div>
        <div key={1}>
            <h4>SCENARIO "C"</h4>
            <h5>Input Conditions</h5>
            <p>1) Yes</p>
            <p>2) Yes</p>
            <p>3) No</p>
            <h5>Output Assertions</h5>
            <p>i) NOT</p>
            <p>ii) MAY</p>
        </div>
    </div>,
     <div>
     <div key={2}>
        <h4>SCENARIO "G"</h4>
        <h5>Input Conditions</h5>
        <p>1) Yes</p>
        <p>2) Yes</p>
        <p>3) Yes-AND-No</p>
        <h5>Output Assertions</h5>
        <p>i) MUST</p>
        <p>ii) MAY</p>
     </div>
    </div>,
     <div>
     <div key={3}>
        <h4>SCENARIO "L"</h4>
        <h5>Input Conditions</h5>
        <p>1) Yes</p>
        <p>2) Yes-AND-No</p>
        <p>3) No</p>
        <h5>Output Assertions</h5>
        <p>i) MAY</p>
        <p>ii) SHOULD</p>
     </div>
    </div>,
    <div>
    <div key={4}>
       <h4>SCENARIO "N"</h4>
       <h5>Input Conditions</h5>
       <p>1) Yes</p>
       <p>2) Yes-OR-No</p>
       <p>3) Yes</p>
       <h5>Output Assertions</h5>
       <p>i) SHOULD</p>
       <p>ii) MUST</p>
    </div>
   </div>,

   
]

export default function Hero() {
    return(
        
        <div className={style.feathold}>
            <div className={style.writinghold}>
                <div>
                    <p ><span className={style.toplabel}>100% Free/libre/open</span> <br />
                    <br /></p>
                    </div>
                    <div className={style.refbox}>
                    <h1 style={{fontWeight: 700}}>Data With Direction.</h1>
                    <p className={style.parastyle}>    Expressions of obligation, permission or encouragement constitute a distinct class of data of the type:
                        <br /><br />  <code>IS + RULE ⟾ OUGHT</code>
                    <br />
                    <br />
                    Instantaneously discover and transmit rules-as-data in a way that is directly usable by anyone and by any platform, for any purpose, in any language.
                    </p>
                
                    <div className={style.labelhold}>
                        <p>Reference Implementations</p>
                    </div>
                    <div className={style.refhold}>
                        <div>
                            <h5 className={style.t}>Rule Taker</h5>
                            <p className={style.p}>The rules you want to know about, now.</p>
                            {/*
                            <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/oughtomation-paper/-/tree/master/rt-sequence-diagram" target="_blank">Sequence Diagram   →</a>
                            */}
                        </div>

                        <div  className={style.hline}/>

                        <div id={style.bar}>
                            <h5 className={style.t}>Rule Reserve</h5>
                            <p className={style.p}>Where rules go to be found.</p>
                            {/*
                            <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/oughtomation-paper/-/tree/master/rr-sequence-diagram" target="_blank">Sequence Diagram  →</a>*/}
                        </div>

                        <div  className={style.hline}/>
                        <div>
                            <h5 className={style.t}>Rule Maker</h5>
                            <p className={style.p}>Write the rules you want to see.</p>
                            {/*
                            <a className={style.a} href="https://gitlab.com/xalgorithms-alliance/oughtomation-paper/-/tree/master/rm-sequence-diagram" target="_blank">Sequence Diagram  →</a>
                            */}
                        </div>
                    </div>
                    <div style={{
                        display: 'grid',
                        gridTemplateColumns: 'repeat(auto-fit, minmax(10em, 1fr))',
                        gridAutoRows: '5em', // Set a fixed row height
                        gap: '10px' // Optional: space between grid items
                    }} >
                    <Link href="https://rulemaker3-dev.onrender.com" >
                        <a target="_blank">
                        <button className={style.altbutton} style={{ 'height' : '4em', 'width': '10em'}} >Try RuleMaker</button>
                        </a>
                    </Link>
                    <Link href="/white-paper" >
                        <a target="_blank">
                        <button className={style.altbutton} style={{ 'height' : '4em', 'width': '10em'}} >Read the Specification</button>
                        </a>
                    </Link>
                    <Link href="https://gitlab.com/xalgorithms-alliance/data-with-direction-specification/dwds-documents/-/blob/master/current/DWDS%20Doctoral%20Thesis%20Defense%202023-01-12PDF.pdf?ref_type=heads" >
                        <a target="_blank">
                        <button className={style.altbutton}  style={{ 'height' : '4em', 'width': '10em'}} >Overview</button>
                        </a>
                    </Link>
                    </div>
                </div>
            </div>
            <div>
               
                <div style={{'min-width': '100%', 'min-height': '100%'}}>
                    <ComponentSlider items={logic_gate_images} interval={15000} animate ={false}/>
                </div>
                <div style={{display: 'flex', maxHeight: '15%'}}>
                    <div style={{width: '70%'}}>
                        <Image
                           src="/logic_gate/symbols_key.png"
                            alt="Symbols Key"
                            width={933}       // Original width of the image
                            height={438}      // Original height of the image
                            layout="responsive" // Makes the image responsive
                            quality={100}     // Maximum quality
                            sizes="(max-width: 600px) 100vw, (max-width: 900px) 50vw, 600px" // Adjust sizes for responsiveness                                objectFit="contain"
                        />        
                    </div>
                    <div style={{width: '30%',  textAlign: 'center'}} className={style.scenarioText}>
                        <ComponentSlider  items={logic_text} animate={false} interval={15000}/>
                    </div>
                </div>   
                
            </div>
        </div>

    )
}