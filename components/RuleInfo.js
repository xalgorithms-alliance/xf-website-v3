import style from './RuleInfo.module.css'
import { Button } from 'xf-material-components/package/index'

export default function RuleInfo() {
    return(<div>
        <div className={style.buildhold}>
            <div>
                 <h2>Obligation, Permission and Encouragement</h2><br/>
                <h4> expressed with terms </h4><br/>
                <h2>MUST, MAY and SHOULD</h2><br/>
                <h4> and their various negatives and synonyms</h4><br/>
            </div>
            
          
        </div>
        <div className={style.dualholdc}>
        <div className={style.dualmodc} style={{'padding-left': '2em', 'text-align': 'center'}}>
                
                <h1 >Rules-as-Data</h1><br/>
                <h4 >
                By Anyone,<br/>
                For Any purpose,<br/>
                On Any platform,<br/>
                In Any language.<br/>
                </h4>
            </div>
            <div className={style.hline}></div>
                <div  className={style.dualmodc}>
                <h3 style={{ 'text-align': 'center'}}>Fast functional knowledge of rules that are deemed to be:</h3><br/>
                    <ul className={style.parastyle} style={{'padding-left': '2em', 'text-align': 'left'}}>
                    <li>‘in effect’ for dates/times and jurisdictions;</li>
                    <li>‘applicable’ to your industry, product or service context; and</li>
                    <li>‘invoked’ by a particular circumstance of the moment.</li>
                    </ul>

                </div>
        </div>
        <div className={style.buildhold}>
            
            <h2>What is a rule?</h2><br/>
                    <p className={style.parastyle} > Assertion of a practical, logical, ethical or aesthetic relation
                            between what ‘is’ and what ‘ought’ to be,
                        established among two or more individuals or entities. </p><br/>
                        <code>IS + RULE ⟾ OUGHT</code><br/><br/>
        </div>
        
        <div className={style.dualholdc}>
                <div className={style.dualmodc}>
                        <h3>"Data With Direction Specification" (DWDS)</h3><br/>
                            <span style={{ 'text-align': 'left'}}>
                                <h5>The RuleMaker application</h5>
                                <li className={style.parastyle} style={{'padding-left': '2em'}}>Simplify, transcribe, and publish rules.</li>   
                                <h5>The RuleReserve network service</h5>
                                <li className={style.parastyle} style={{'padding-left': '2em'}}>Store, sift, and transmit rules-as-data.</li>
                                <h5>The RuleTaker component</h5>
                                <li className={style.parastyle} style={{'padding-left': '2em'}}>Discover, fetch, scrutinize, and use rules.</li>
                            </span>                            
                </div>
                <div className={style.hline}></div>
                <div className={style.dualmodc}>
                <h3 >A Practicable Business Solution:</h3><br/>
                                <p className={style.parastyle}>
                                A DWDS describes how to create 'an Internet of Rules': a decentralized distributed network service for anyone to author, publish, discover, fetch, scrutinize, prioritize and, with agreement of direct stakeholders, automate rules that are ‘in effect’, ‘applicable’ and ‘invoked’ by a circumstance, across any informatics network, with precision, simplicity, scale, speed, resilience, and deference.
                                </p>                       
                </div>
               
        </div>
        <div className={style.buildhold}>
            
        <h3 >The Network <br/>Effect</h3><br/>
        <span style={{'text-align': 'left'}}>
        <p className={style.parastyle}>
        The value of 'an Internet of Rules' to any individual user increases as there is greater adoption across the entire user base:
        </p>

        <ul className={style.parastyle} style={{'padding-left': '2em',}}>
            <li>Greater performance by each is more attainable the more RuleReserve gets populated by all;</li>
            <li>Distribution of effort enables economies of scale (lower per-user cost; spread cost of innovation); and</li>
            <li>Joint benefits of a generic network usable by any platform (write once, run anywhere).</li>
        </ul>
        </span>
        </div>
        
        </div>
    )
}
