import style from './NavGetInvolved.module.css'
import Persona from './Persona'
import Link from 'next/dist/client/link'

export default function NavGetInvolved() {
    return(
        <div className={style.triflex}>
            <div>
                <div style={{backgroundImage: 'url("/nav-img-involved.png")', width: '360px', backgroundSize: 'cover', backgroundPosition: 'center', marginBottom: '1em', height: '200px' }} />
                <p>
                Face complexity. Discover intuitive solutions.
                </p>
            </div>
            <div className={style.hline}/>
            <div>
                <Link href="/get-involved">
                    <a className={style.a}>Xalgorithms Foundation Stakeholder Roles and Working Assumptions About their Market Revenues and Costs.</a>
                </Link>
                <div className={style.hrule}/>
                
            </div>
        </div>
    )
}