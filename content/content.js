   const Content =  {english: {
        landing: {
          hook: "Practical Normative Data.",
          cta: "An Invitation to Collaborate",
          problem: "How can you discover and obtain facts about the normative constraints that are 'in effect', 'applicable' and 'invoked' for any undertaking at any given moment?",
          problemCta: "Read the White Paper  →",
          solution: "Until now the world has lacked a common efficient way to communicate compulsion, option or expectation from rule-makers to rule-takers.",
          solutionCta: "See XRM dev  →",
          foundation: `The Xalgorithms community has created a new general-purpose request-response messaging system under free/libre/open licensing for use across any digital network.
          <br/>
          This will make it easy for anyone to publish, discover, fetch, scrutinize and prioritize normative rules (MUST, MAY and SHOULD) in a way that can be directly read and understood by non-specialized humans and machines, for any purpose, in any language. An 'Internet of Rules' will enhance the functional communication of IS ⟾ OUGHT assertions with precision, simplicity, scale, volume and speed, and with deference to jurisdictional prerogative.
          <br/>
          This enables diverse novel use cases in trade and commerce, mechatronics, money and finance, ecological management, industrial control systems, digital learning, and other domains involving  assertions amongst individual and organizational agents.`
        }
    }
  }

  export default Content