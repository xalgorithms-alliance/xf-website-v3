import Build from '../components/Build'
import Learn from '../components/Learn'
import FullFeatWriting from '../components/FullFeatWriting'
import TitleMod from '../components/TitleMod'
import { Button } from 'xf-material-components/package/index'
import Date from '../components/Date'
import SEO from '../components/SEO'
import StakeHolders from '../components/StakeHolders'

export default function Involved() {
    return(
        <>
        <SEO title="Let's Build" description="A community building a simple, scalable, free open source way to publish, discover, fetch, scrutinize and prioritize rules-as-data." img="seo.png"/>
        <div style={{height: '6em'}} />
      
        <TitleMod title="Let's Build." img="writing-images/journey.jpg"/>
        <div className="communityFlex">

        </div>
        <StakeHolders />
        <Learn/>
        </>
    )
}