import Head from 'next/head'
import usesContent from '../content/usesContent'
import SignUp from '../components/SignUp'
import { Column } from 'xf-material-components/package/index'

import SEO from '../components/SEO'

import { useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'



export default function uses() {

  const lang = useContext(LanguageSelect)
  const content = usesContent[lang]


  const renderContent = () => {
    return content.uses.map(({ id, title, content, links = []}, index) => (
      <div className="use" key={index}>
        <h4>{title}</h4>
        <div id={id} className="useGrid">
          <div>
            <p>{content}</p>
          </div>
          {/*}
          <div className="linkHold">
            <h4>Links</h4>
            {renderLinks(links)}
          </div>
        */}
        </div>
      </div>
    ));
  };

    return (
      <>
      <SEO title="The Xalgorithms Foundation | Uses" description="A community building a simple, scalable, free open source way to publish, discover, fetch, scrutinize and prioritize rules-as-data."/>
      <div className="container">
        <main>
        <div className="hero">
        <div className="WritingGrid12" id="bottomMargin">
          <div className="sideCTA" >
            <h2>{content.headline}</h2>
          </div>
            <div className="animationTwo" className="seven-eight" >
              <p>
                <br />
              </p>
              <Column>
                {renderContent()}
              </Column>
            </div>
        </div>
        </div>
        </main>
      </div>
      </>
    )
  }