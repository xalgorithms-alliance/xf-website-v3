import Build from '../components/Build'
import Fintech from '../components/Fintech'
import Learn from '../components/Learn'
import FullFeatWriting from '../components/FullFeatWriting'
import TitleMod from '../components/TitleMod'
import { Button } from 'xf-material-components/package/index'
import Date from '../components/Date'
import SEO from '../components/SEO'
import StakeHolders from '../components/StakeHolders'

export default function FintechAward() {
    return(
        <>
        <SEO title="Finnovator for 2024" description="Xalgorithms Foundation Wins Finnovator for 2024 Award at Central Banking Autumn Meetings" img="seo.png"/>
        <div style={{height: '6em'}} />
      
        <TitleMod title="Xalgorithms Foundation Wins Finnovator for 2024 Award at Central Banking Autumn Meetings" img="fintech/awardAcceptance.jpg" fixedImageSize={true}/>
        <div className="communityFlex">

        </div>
        <Fintech />
        <Learn/>
        </>
    )
}