import React, {  useEffect, useRef, useState } from 'react';
import Link from 'next/link'
import { getSortedPostsData } from '../lib/posts'
import Tag from '../components/Tag'
import { useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'
import content from '../content/content'
import SEO from '../components/SEO'
import FoundationCall from '../components/FoundationCall'
import Learn from '../components/Learn';
import Build from '../components/Build'
import FullFeatWriting from '../components/FullFeatWriting'
import UsecaseCall from '../components/UsecaseCall';
import Hero from '../components/Hero';
import Graphics from '../components/Graphics';
import RuleInfo from '../components/RuleInfo'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}

export default function Home({ allPostsData }) {

  // gets content for the selected language
  const lang = useContext(LanguageSelect)
  const copy = content[lang]

  const mainhold = {
    display: 'flex',
    alignItems: 'flex-end',
    minHeight: '60vh',
    paddingTop: '81px',
    justifyContent: 'space-between'
  }

  const writingTimeHold = {
    display: 'grid',
    width: '100vw',
    gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr',
    alignItems: 'center',
    marginBottom: '4em'
  }

  const marginal = {
    marginTop: '0.5em',
    marginBottom: '0.5em'
  }

  const ahold = {
    gridArea: '1 / 2 / 2 / 6',
  }

  const imghold = {
    overflow: 'hidden',
    gridArea: '1 / 7 / 2 / 12',
    width: '100%',
    height: '400px'
  }

  const present = {
    width: '100%'
  }

  const writingH = {
    display: 'grid',
    width: '100%',
    gridTemplateColumns: '1fr 1fr 1fr'
  }

  const width = {
    gridArea: '1 / 1 / 2 / 13',
  }

  const middleline = {
    width: '200px',
    margin: 'auto',
    textAlign: 'center',
    marginBottom: '4em'
  }
  const paddingunit = {
    height: '0em'
  }

  const bgimg = {
    backgroundImage: "url(./test-feat.png)",
    width: "100%",
    height: "600px",
    backgroundSize: "cover",
    backgroundPosition: "center",
  }

  const lilmargin = {
    marginBottom: '2em',
    maxWidth: '500px',
    minWidth: '200px',
    flexShrink: '0',
    flexGrow: '1',
    flexBasis: '30%'
  }

  const centerText = {
    textAlign: 'center',
  }

  
  
  // retrieve featured posts

  const renderPost = () => {
    return(allPostsData.map(({ id, title, featured, featimg, tags=[] }, index) => (
      featured ? ( 
        <div key={index} className="indexWritingChild">
          <div style={{backgroundImage: "url(/" + featimg + ")", width: "100%", backgroundSize: "cover", backgroundPosition: "center center", marginBottom: '1em'}} className='picHeight'/>
            {tags.map(({ tag }, index) => (
              <Tag target={tag} key={index} />
            ))}
            <p className="featuredtitle">
              {title}
            </p>
            <Link href={'/writing/'+id}>
                <a className="noUnderline">
                  Read   →
                </a>
            </Link>
        </div>
      ) : ( 
        null
      )
      
    )))
  }
  

  return (
    <>
    <SEO title="The Xalgorithms Foundation" description="A community building a simple, scalable, free open source way to publish, discover, fetch, scrutinize and prioritize rules-as-data." img="seo.png"/>
    <Hero />
    
    <Graphics />
    <RuleInfo />
    <FoundationCall />
    <FullFeatWriting title="The Data With Direction Specification (DWDS) in a Nutshell" target="/writing/dwds-in-a-nutshell" img="writing-images/RULE-OUGHT.png"/>
    <Learn />
    <UsecaseCall />

    
    </>
          
  )
}
