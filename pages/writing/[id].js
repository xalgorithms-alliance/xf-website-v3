import AboutAuthor from '../../components/AboutAuthor'
import Tags from '../../components/Tags'
import { getAllPostIds, getPostData } from '../../lib/posts'
import { useContext } from 'react'
import { Breadcrumbs } from '../../components/Bcrumb'
import renderToString from 'next-mdx-remote/render-to-string';
import hydrate from 'next-mdx-remote/hydrate';
import { Button } from 'xf-material-components/package/index'
import Logo from '../../components/Logo'
import SEO from '../../components/SEO'
import Caption from '../../components/Caption'
import SideQuote from '../../components/SideQuote'
import communityContent from '../../content/communityContent'
import LanguageSelect from '../../lib/languageSelect'
import RandomLogo from '../../components/RandomLogo'
import WritingLink from '../../components/WritingLink'

const components = { Button, Logo, Caption, SideQuote, RandomLogo, WritingLink }

export async function getStaticPaths() {

    const paths = getAllPostIds()
    return {
      paths,
      fallback: false
    }
  }

  export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id) 
    const mdxSource = await renderToString(postData.mdxPath, { components })
    return {
      props: {
        postData,
        source: mdxSource,
      }
    }
  }

  const widtht = {
    display: 'block'
  }

  

  

export default function Post({ postData, source }) {
  const lang = useContext(LanguageSelect)
  const authorContent = communityContent[lang].team

  const renderTeam = () => {
    return authorContent.map(({img, name, bio, links}, index ) => (
      (name === postData.author) ? (
        <AboutAuthor image={img} name={name} description={bio} links={links} key={index}/>
      ) : (
        null
      )
    ));
  }

    const bgColor = 'var(--firmament)'

    const bcrumb = Breadcrumbs()

    const content = hydrate(source, { components });


    

    const bgimg = {
      backgroundImage: "url(/" + postData.featimg + ")",
      backgroundSize: "cover",
      backgroundPosition: "center center",
    }

    return (
      <div className="container">
        <SEO img={postData.featimg} title={postData.title} tags={postData.tags}/>
        <main>
          <div style={widtht}>
            <div className="grid12">
              <div style={bgimg} className="imghold">
              </div>
            </div>
          </div>
          <div className="singlecol">
            <div style={widtht}>
              <div className="WritingGrid12" id="writingTopMargin">
                <div className="writingGridHold">
                  <h2>{postData.title}</h2>
                    <div className="textm">
                          {content}
                    </div>
                  <div className="ctaHold">
                  </div>
                </div>
                <div className="sticky">
                  <div className="appearance">
                    <div>{bcrumb}</div>
                    <Tags tags={postData.tags} />
                  </div>
                  {renderTeam()}
                </div>
              <div />
            </div>
            </div>
          </div>
        </main>
      </div>
    )
  }

  