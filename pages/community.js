import Head from 'next/head'
import AboutAuthor from '../components/AboutAuthor'
import communityContent from '../content/communityContent'

import SEO from '../components/SEO'

import { useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'

import Date from '../components/Date'

import { Button } from 'xf-material-components/package/index'

export default function community() {
  const lang = useContext(LanguageSelect)
  const content = communityContent[lang]

  const topGrid = {
    gridArea : "1 / 2 / 1 / 6"
  }

  const sideGrid = {
    gridArea : "1 / 8 / 1 / 11",
    alignSelf: "center"
  }

  const img = {
    width: '100%'
  }

  const grida = {
    gridArea : "1 / 2 / 1 / 4"
  }

  const gridb = {
    gridArea : "1 / 5 / 1 / 6"
  }

  const gridc = {
    gridArea : "1 / 8 / 1 / 9"
  }

  const linkmargin = {
    paddingTop: "1em"
  }

  const small = {
    border: 'none',
    background: 'none',
    color: 'var(--steel)',
    fontSize: '0.65em',
    textTransform: 'uppercase',
    textAlign: "center",
    marginTop: '2em',
    marginBottom: '2em'
  }

  const renderTeam = () => {
    return content.team.map(({img, name, bio, links}, index ) => (
      img ? (
      <div className="indexWritingChild" key={index}>
        
        <AboutAuthor image={img} name={name} description={bio} links={links}/>
      </div>
       ) : ( 
        null
      )
    ));
  }

  const renderWorkingGroups = () => {
    return content.workingGroups.map(({label, target}, index) => (
      <div style={linkmargin}>
        <a className="noUnderline" href={target} key={index}>
            {label} →
        </a>
      </div>
    ))
  }

    return (
      <>
      <SEO title="Community" description="Ideas Bounce! They Really do." img="seo.png"/>
        <main>
          <section>
            <div>
              <div>
                <div style={{textAlign: "center"}}>
                  <h2>{content.headline}</h2>
                  {/*
                  <h5>Working Groups</h5>
                    {renderWorkingGroups()}
                  */}
                </div>
                <div >
                  
                  <p>
                    <br />
                  </p>
                  
                  <div className="communityFlex">
        <div className="communityCtaHold">
          <div style={{backgroundImage: "url(community.png)", width: "100%", backgroundSize: "cover", backgroundPosition: "center", marginBottom: '1em'}} className='ctaPicHeight'/>
          <div>
            <h4 >
            Did you know that ideas bounce?
            <br />
            <br />
They do. Really! 
            </h4>
            
          </div>
        </div>
      </div>
                
                  <br />
                  <div className="communityFlex">  
                    <div className="indexWritingHold">     
                      {renderTeam()}
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </section>
        </main>
      </>
    )
  }