// import { redirect } from 'next/dist/next-server/server/api-utils'
import Link from 'next/link'
import { getSortedPostsData } from '../lib/posts'
import sortTags from '../lib/sortTags'
import Tag from '../components/Tag'
import SignUp from '../components/SignUp'
import { useEffect, useState, useContext } from 'react'
import tagSelect from '../lib/tagSelect'

import SEO from '../components/SEO'


export async function getStaticProps() {

  

  const allPostsData = getSortedPostsData()
    return {
      props: {
        allPostsData
      }
    }
  }

export default function writing ({ allPostsData }) {
  const tagFlex = {
    display: 'flex',
    justifyContent: 'space-between'
  }

  const small = {
    border: 'none',
    background: 'none',
    color: 'var(--steel)',
    fontSize: '0.65em',
    textTransform: 'uppercase',
    cursor: 'pointer',
    marginLeft: '1em'
  }

  const littleMargin = {
    marginBottom: '2em',
    textTransform: 'capitalize',
    textAlign: 'center'
  }

  const bottom = {
    marginLeft: '1em'
  }


  const[tagArray, setTagArray] = useState([]);

  const renderTitle = () => {
    return(
      (globaltag === "all") ? (
        <>
        <div style={littleMargin}>
                    <h2 className="sectionTitle">{globaltag} Writing</h2>
        </div>
        <div style={littleMargin}>
            <button
                    style={small}
                    onClick={resetFilter}
                  >
              <svg width="12" height="10" viewBox="0 0 12 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4.83862 4.5354L0.8125 0.509277H11.1875L7.16138 4.5354V7.16786L4.83862 9.49062V4.5354Z" stroke="var(--steel)" strokeLinecap="round" strokeLinejoin="round"/>
              </svg>
                  Filter by tag
          </button>
        </div>
        </>
      ) : (
        <>
        <div style={littleMargin}>
            <h2>{globaltag} Writing</h2>
        </div>
        <div style={littleMargin}>
            <button
                    style={small}
                    onClick={resetFilter}
                  >
              <svg width="12" height="10" viewBox="0 0 12 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4.83862 4.5354L0.8125 0.509277H11.1875L7.16138 4.5354V7.16786L4.83862 9.49062V4.5354Z" stroke="var(--steel)" strokeLinecap="round" strokeLinejoin="round"/>
              </svg>
                  Clear Filter
          </button>
        </div>
        </>
      )
    )
  }

  const renderPost = () => {
    
    return(
      (globaltag === "all") ? (
      <>
        <>
          {allPostsData.map(({ id, title, published, featimg, tags=[] }) => (
          (published === "true") ? ( 
            <div style={{marginBottom: "2em"}} className="indexWritingChild" key={id}>
              <div style={{backgroundImage: "url(/" + featimg + ")", width: "100%", backgroundSize: "cover", backgroundPosition: "center center", marginBottom: '1em'}} className="picHeight"/>
                {tags.map(({ tag }, index) => (
                  <Tag target={tag} key={index} />
                ))}
                <p className="featuredtitle">
                  {title}
                </p>
                <Link href={'/writing/'+id}>
                    <a className="noUnderline">
                      Read   →
                    </a>
                </Link>
            </div>
          ) : ( 
            null
          )
        ))}
      </>
    </>
    ) : (
      <>
        <>
        {allPostsData.map(({ id, title, published, featimg, tags=[] }) => (
          tags.map(({tag}) => (
          (tag === globaltag) ? ( 
            published ? (
            <div key={id} className="indexWritingChild" style={{marginBottom: "2em"}}>
              <div style={{backgroundImage: "url(/" + featimg + ")", width: "100%", height: "18vw", backgroundSize: "cover", backgroundPosition: "center center", marginBottom: '1em'}}/>
                {tags.map(({ tag }, index) => (
                  <Tag target={tag} key={index} />
                ))}
                <p className="featuredtitle">
                  {title}
                </p>
                <Link href={'/writing/'+id}>
                    <a className="noUnderline">
                      Read   →
                    </a>
                </Link>
            </div>
            ) : (
              null
            )
          ) : ( 
            null
          )
          ))
        ))}
        </>
      </>
    )
    )
  }

  const getAllTags = () => {
    allPostsData.map(({published, tags=[]}) => {
      published ? (
        tags.map(({tag}) => {
          setTagArray(tagArray => tagArray.concat(tag))
        })
       ) : (
        null
       )
    })
  }
  
  
  const renderTags = () => {
    const sortedTags = [...new Set(tagArray)]
    return(
      sortedTags.map((index ) => (
        <Tag target={index} key={index} />
      ))
    )
  }

  
  useEffect(() => {
    getAllTags()
  }, [])

  const { globaltag, filterTag } = useContext(
    tagSelect
  );

  const resetFilter = () => {
    filterTag("all")
  }
  

    return (
     <>
      <SEO title="The Xalgorithms Foundation | Writing" description="A community building a simple, scalable, free open source way to publish, discover, fetch, scrutinize and prioritize rules-as-data." img="seo.png"/>
        <main>
          <section>
            <div>
              <div style={{textAlign: "center"}}>
                {renderTitle()}
                
                <div className="tagsHold">
                  {renderTags()}
                </div>
              </div>
              <div className="indexWritingHold">
                {renderPost()}
                <div style={bottom}>
                </div>
              </div>
            </div>
          </section>
        </main>
      </>
    )
  }